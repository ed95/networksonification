- The file path for the STK modules must be added to the JUCE project for it to function properly, they are included in the JuceLibraryCode/modules folder and adhere to the new JUCE 4.2.0 module standard

- Libpcap is included in the /source/libpcap folder in case it is not already installed