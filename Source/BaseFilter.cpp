/*
  ==============================================================================

    BaseFilter.cpp
    Created: 18 Dec 2015 1:54:22pm
    Author:  Edward Davies

  ==============================================================================
*/

#include "BaseFilter.h"

BaseFilter::BaseFilter (Audio& audio_) : audio (audio_)
{
    filter.clear();
    qVal = audio.filterQ->getDefaultValue();
}

BaseFilter::~BaseFilter()
{
    
}

float BaseFilter::getQ()
{
    qVal = audio.filterQ->getValue();
    return qVal;
}

float BaseFilter::getSampleRate()
{
    return audio.sampleRate->getValue();
}

void BaseFilter::setCoeffs (float b0, float b1, float b2, float a1, float a2)
{
    filter.setB0(b0);
    filter.setB1(b1);
    filter.setB2(b2);
    
    filter.setA1(a1);
    filter.setA2(a2);
}

float BaseFilter::outputFiltered (float inputSample)
{
    return filter.tick (inputSample);
}