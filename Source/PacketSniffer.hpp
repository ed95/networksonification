//
//  PacketSniffer.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/12/2015.
//
//

#ifndef PacketSniffer_hpp
#define PacketSniffer_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PacketParser.hpp"

/**
    Packet sniffer class that captures packets from the network interface 
    hardware and passes them to the PacketParser class to be parsed.
 */
class PacketSniffer : Thread
{
public:
    /**
        Constructor.
        @param parser_ Reference to the packet parser class. @see PacketParser
     */
    PacketSniffer (PacketParser& parser_);
    
    /**
        Destructor.
     */
    ~PacketSniffer();
    
    /**
        Initialises the packet sniffer.
        @param captureDevice The network device to capture on
        @param filterExpression The capture filter expression (if any)
        @param promiscMode Opens the capture session in promiscuous mode if true
        @param info Pointer to the network info struct to fill
        @param audioMode The audio mode to start in
        @return Returns an int other than 0 if there was an error opening the capture session
     */
    int initialise (const char* captureDevice, const char* filterExpression, bool promiscMode, NetworkInfo* info);
    
    /**
        Sets the state of the packet sniffer.
        @param state Starts the packet capture thread if true, stops it if false
     */
    void setSnifferState (bool state);
    
    /**
        Gets the state of the sniffer.
        @return true if sniffer is running, false if not
     */
    bool getSnifferState();

    /**
        Checks whether the sniffer has been initialised successfully.
        @return True if initialised with no errors, false if errors
     */
    bool isSnifferInitialised();
    
    /**
        Sets whether promiscuous mode should be on.
        @param on True if promiscuous mode should be turned on
        @return Zero if no errors, error code if errors
     */
    int setPromiscuousMode (bool on);
    
    /** 
        Inherited from Thread
     */
    void run() override;

    /**
        Returns a reference to the packet parser @see PacketParser
     */
    PacketParser& getParser() { return parser; };
    
    /**
        Returns a String array filled with the available device types
     */
    Array<String> getAvailableDeviceTypes();
    
private:
    const char* dev;                /// The device to capture on
    const char* filter;             /// The filter expression to use
    pcap_t* handle;                 /// The handle
    char errBuff[PCAP_ERRBUF_SIZE]; /// Error buffer to fill
    bpf_u_int32 net;                /// The network address
    bpf_u_int32 mask;               /// IP mask
    struct bpf_program fp;          /// Filter
    
    bool snifferState;    /// Holds the state of the sniffer
    PacketParser& parser; /// Reference to the packet parser
    int packetCount;      /// Keeps count of how many packets have been captured in the session
    
    bool initialised; /// True if the sniffer is initialised correctly
    
    Array<String> deviceNameArray; /// String array of available device names
};
#endif /* PacketSniffer_hpp */
