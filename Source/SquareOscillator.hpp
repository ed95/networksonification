//
//  SquareOscillator.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 16/12/2015.
//
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

#include <stdio.h>
#include "BaseOscillator.hpp"

/**
    Square wave sound.
 */
class SquareSound : public SynthesiserSound
{
public:
    /**
         Constructor.
     */
    SquareSound() {};
    
    /**
        Destructor.
     */
    ~SquareSound() {};
    
    /**
         Inherited from SynthesiserSound. Applies to all notes.
     */
    bool appliesToNote (int midiNoteNumber) override { return true; };
    
    /**
         Inherited from SynthesiserSound. Applies to MIDI channel 4.
     */
    bool appliesToChannel (int midiChannel) override
    {
        if (midiChannel == 3)
            return true;
        return false;
    };
    
private:
    int protocol;
};

/**
    Square wave voice.
 */
class SquareVoice : public BaseOscillator
{
public:
    
    /**
         Constructor.
         @param Reference to the Audio class. @see Audio
     */
    SquareVoice (Audio& audio_);
    
    /**
         Destructor.
     */
    ~SquareVoice();
    
    /**
         Returns true if the voice can play a sound.
     */
    bool canPlaySound (SynthesiserSound* sound) override;
    
    /**
         Sets the frequency of the STK generator. Inherited from BaseOscillator.
         @param frequency The frequency to set the generator
     */
    void setSTKFreq (float frequency) override;
    
    /**
         Resets the STK generator. Inherited from BaseOscillator.
     */
    void resetSTKGen() override;
    
    /**
         Renders the waveshape. Inherited from BaseOscillator.
         @return The sample value for this waveshape
     */
    float renderWaveshape() override;
    
    /**
         Sets whether vibrato should be on or off for this voice. Inherited from BaseOscillator.
         @param isOn True if vibrato should be on, false if off
     */
    void vibratoOn (bool isOn) override;
    
    /**
         Sets the rate and depth of the vibrato effect. Inherited from BaseOscillator.
         @param vibRate The rate in Hz of the vibrato LFO
         @param vibDepth The relative depth of the effect between 0 and 1
     */
    void setVibRateDepth (float vibRate, float vibDepth) override;
    
private:
    stk::BlitSquare squareGen; /// STK band-limited square wave generator
    
    stk::SineWave vibGen;      /// Vibrato LFO
    bool isVibratoOn;          /// True if vibrato effect should be applied, false if not
    float baseFreq;            /// Stores the base frequency of the note
    float vDepth;              /// Relative depth of the vibrato effect
    float vRate;               /// Rate in Hz of the vibrato LFO
};
#endif /* SquareOscillator_hpp */
