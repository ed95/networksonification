//
//  PacketParser.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 06/12/2015.
//
//

#include "PacketParser.hpp"

/**
    Callback class for updating the graphics component.
 */
class UpdateGraphicsCallback : public CallbackMessage
{
public:
    UpdateGraphicsCallback (PacketParser* o)
    : owner (o)
    {}
    
    void messageCallback() override
    {
        if (owner != nullptr)
            owner->updateGraphics();
    }
    
    Component::SafePointer<PacketParser> owner;
};

/**
     Callback class for printing a string to the text component.
 */
class PrintStringCallback : public CallbackMessage
{
public:
    PrintStringCallback (PacketParser* o, String packetString_)
    : owner (o), packetString (packetString_)
    {}
    
    void messageCallback() override
    {
        if (owner != nullptr)
            owner->printString(packetString);
    }
    
    Component::SafePointer<PacketParser> owner;
    String packetString;
};

PacketParser::PacketParser (Audio& audio_, PacketGraphicsComponent& graphicsComp_, TextComponent& textComp_)
                          : audio (audio_), graphicsComp (graphicsComp_), textComp (textComp_)
{
    for (int i = 0; i < 4; ++i)
    {
        graphicTriggerLocks[i] = false;
        zerostruct (packetToVisualise[i]);
        packetToVisualise[i].packetType = -1;
    }
    
    listener = nullptr;
    
    outputString.clear();
    ipListString.clear();
    statsString.clear();
    
    shouldPrint = false;
    
    IPAddress::findAllAddresses (ipAddresses);
    MACAddress::findAllAddresses (macAddresses);
    
    broadcastMAC = MACAddress (StringRef ("FFFFFFFFFFFF"));
    broadcastIP  = IPAddress (String ("255.255.255.255"));
    
    IPConnections.deleteAll();
    currentTCPConnections.deleteAll();
    random.setSeed (Time::getCurrentTime().toMilliseconds());
    
    //fill arrays with known port numbers and their corresponding names
    File location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory()
    .getChildFile ("Resources").getChildFile ("resources");
    
    if (!location.exists())
    {
        location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile ("resources");
    }
    
    FileInputStream udpNamesStream (location.getChildFile ("ports/udp_portnames"));
    FileInputStream udpNumsStream (location.getChildFile ("ports/udp_portnums"));
    
    FileInputStream tcpNamesStream (location.getChildFile ("ports/tcp_portnames"));
    FileInputStream tcpNumsStream (location.getChildFile ("ports/tcp_portnums"));
    
    while (!udpNamesStream.isExhausted())
        udpPortNames.add (udpNamesStream.readNextLine());
    while (!udpNumsStream.isExhausted())
        udpPortNumbers.add (udpNumsStream.readNextLine().getIntValue());
    
    while (!tcpNamesStream.isExhausted())
        tcpPortNames.add (tcpNamesStream.readNextLine());
    while (!tcpNumsStream.isExhausted())
        tcpPortNumbers.add (tcpNumsStream.readNextLine().getIntValue());
    
    zeroCounters();
}

PacketParser::~PacketParser()
{
    
}

void PacketParser::parsePacket (int packetCount, const struct pcap_pkthdr pkthdr, const u_char *packet)
{
    //clear the output string for the new packet and add number
    outputString.clear();
    outputString << packetCount << ". ";
    
    shouldPrint = textComp.isActive();
    
    PacketData pData;
    zerostruct (pData);
    
    u_int16_t type = handleEthernet (packet);
    if (type == ETHERTYPE_IP)
    {
        handleIP (packet, pkthdr.caplen, pData);
        
        if (shouldPrint)
            (new PrintStringCallback (this, outputString))->post();
    }
    else if (type == ETHERTYPE_ARP || type == ETHERTYPE_REVARP)
    {
        ++arpCount;
        handleARP (packet, pData);
        
        if (shouldPrint)
            (new PrintStringCallback (this, outputString))->post();
    }
}

void PacketParser::didntGrab()
{
    outputString.clear();
    outputString << "Didn't grab packet" << newLine;
    
    if (shouldPrint)
        (new PrintStringCallback (this, outputString))->post();
}

void PacketParser::sessionStats (int pCount, RelativeTime t)
{
    statsString.clear();
    
    statsString << "Captured " << pCount << " packets in "
                << t.inSeconds()  << " seconds."
                << newLine;
    
    statsString << "PPS = " << pCount / t.inSeconds()
                << newLine;
    
    (new PrintStringCallback (this, statsString))->post();
}

void PacketParser::printIPs (bool sorted)
{
    if (sorted)
    {
        sourceIPs.sort (sorter);
        destIPs.sort (sorter);
    }
    
    ipListString.clear();
    
    ipListString << "Source IP addresses:" << newLine;
    
    for (int i = 0; i < sourceIPs.size(); ++i)
        ipListString << i + 1  << ". "
                     << (ipAddresses.contains (sourceIPs[i].address) ? "SELF"
                                                                     : sourceIPs[i].address.toString())
                     << " (" << sourceIPs[i].freq  << ")"
                     << newLine;
    
    ipListString << "Destination IP addresses:" << newLine;
    for (int i = 0; i < destIPs.size(); ++i)
        ipListString << i + 1 << ". "
                     << (ipAddresses.contains (destIPs[i].address) ? "SELF"
                                                                   : destIPs[i].address == broadcastIP ? "BROADCAST"
                                                                                                       : destIPs[i].address.toString())
                     << " (" << destIPs[i].freq << ")"
                     << newLine;
    
    
    (new PrintStringCallback (this, ipListString))->post();
}

void PacketParser::setListener (PacketParser::Listener *newListener)
{
    if (newListener != nullptr)
        listener = newListener;
}

void PacketParser::timerCallback (int timerID)
{
    if (timerID == IPWINDOWID)
    {
        DataToBeMapped data;
        data.t = DataToBeMapped::Int;
        data.iVal = IPConnections.size();
        data.identifier = DataIdentifiers::numIPConnections;
        
        audio.networkDataQueue.addToFifo(&data, 1);
        
        sharedMem.enter();
        IPConnections.deleteAll();
        sharedMem.exit();
    }
    else if (timerID == PPSWINDOWID)
    {
        float arp  = (arpCount - arpLastCount)   / (PPSWINDOWTIME / 1000.f);
        float udp  = (udpCount - udpLastCount)   / (PPSWINDOWTIME / 1000.f);
        float tcp  = (tcpCount - tcpLastCount)   / (PPSWINDOWTIME / 1000.f);
        float icmp = (icmpCount - icmpLastCount) / (PPSWINDOWTIME / 1000.f);
        
        arpPps  = (SMOOTHINGFACTOR * arp)  + ((1.f - SMOOTHINGFACTOR) * arpPps);
        udpPps  = (SMOOTHINGFACTOR * udp)  + ((1.f - SMOOTHINGFACTOR) * udpPps);
        tcpPps  = (SMOOTHINGFACTOR * tcp)  + ((1.f - SMOOTHINGFACTOR) * tcpPps);
        icmpPps = (SMOOTHINGFACTOR * icmp) + ((1.f - SMOOTHINGFACTOR) * icmpPps);
        
        arpLastCount  = arpCount;
        udpLastCount  = udpCount;
        tcpLastCount  = tcpCount;
        icmpLastCount = icmpCount;
        
        float totalPPS = arpPps + udpPps + tcpPps + icmpPps;
        
        DataToBeMapped data;
        data.t = DataToBeMapped::Float;
        
        data.fVal = totalPPS;
        data.identifier = DataIdentifiers::packetsPerSecond;
        audio.networkDataQueue.addToFifo (&data, 1);
        
        data.fVal = arpPps;
        data.identifier = DataIdentifiers::ppsARP;
        audio.networkDataQueue.addToFifo (&data, 1);
        
        data.fVal = tcpPps;
        data.identifier = DataIdentifiers::ppsTCP;
        audio.networkDataQueue.addToFifo (&data, 1);
        
        data.fVal = udpPps;
        data.identifier = DataIdentifiers::ppsUDP;
        audio.networkDataQueue.addToFifo (&data, 1);
        
        data.fVal = icmpPps;
        data.identifier = DataIdentifiers::ppsICMP;
        audio.networkDataQueue.addToFifo (&data, 1);
        
        PPSData pps;
        pps.totalPPS = totalPPS;
        pps.arpPPS = arpPps;
        pps.tcpPPS = tcpPps;
        pps.udpPPS = udpPps;
        pps.icmpPPS = icmpPps;
        
        if (listener != nullptr)
            listener->updateLabels (pps);
    }
    else if (timerID == IPPRINTID)
    {
        printIPs (true);
        stopTimer (IPPRINTID);
    }
    else if (timerID <= ICMPPACKET)
    {
        graphicTriggerLocks[timerID] = false;
        stopTimer (timerID);
    }
    else
    {
        for (int i = 0; i < currentTCPConnections.size(); ++i)
        {
            TCPConnection connection = currentTCPConnections.get (i);
            if (connection.ID == timerID)
            {
                currentTCPConnections.remove (i);
                ++connection.currentStage;
                
                PacketData data;
                data.packetType = TCPPACKET;
                data.TCPID = connection.ID;
                data.TCPDisconnection = true;
                
                routeData (TCPPACKET, data);
                stopTimer (timerID);
            }
        }
    }
}

void PacketParser::setSampleInterval (int ms)
{
    sampleInterval = ms;
}

void PacketParser::captureStarted()
{
    // payload size
    ParameterPair param;
    param.t = ParameterPair::Flag;
    param.identifier = Parameters::pResetPayloadSize;
    audio.audioParametersQueue.addToFifo (&param, 1);
    
    // network data
    sourceIPs.clear();
    destIPs.clear();
    IPConnections.deleteAll();
    currentTCPConnections.deleteAll();
    
    // text output strings
    outputString.clear();
    ipListString.clear();
    statsString.clear();
    
    // components
    graphicsComp.reset();
    textComp.reset();
    
    // protocol counters
    zeroCounters();
    
    // start timers
    startTimer (IPWINDOWID, IPWINDOWTIME);
    startTimer (PPSWINDOWID, PPSWINDOWTIME);
    audio.startTimer (TCPTIMERID, TCPWINDOWTIME);
}

void PacketParser::captureStopped()
{
    // stop all notes
    ParameterPair param;
    param.t = ParameterPair::Flag;
    param.identifier = Parameters::pStopAllNotes;
    audio.audioParametersQueue.addToFifo (&param, 1);
    
    // stop timers
    stopTimer (IPWINDOWID);
    stopTimer (PPSWINDOWID);
    audio.stopTimer (TCPTIMERID);
    
    // start timer for IP printing - wait for thread to exit
    startTimer (IPPRINTID, IPPRINTTIME);
}

//====================================PRIVATE FUNCTIONS==============================================
u_int16_t PacketParser::handleEthernet (const u_char* packet)
{
    const struct sniffEthernet *ethernet;
    u_short etherType;
    
    ethernet = (struct sniffEthernet*)(packet);
    etherType = ntohs (ethernet->ether_type);
    
    MACAddress sourceMAC (ethernet->ether_sHost);
    MACAddress destMAC   (ethernet->ether_dHost);
    
    outputString << "Ethernet header source: " << (macAddresses.contains (sourceMAC) ? "SELF"
                                                                                     : sourceMAC.toString())
                 << " destination: "           << (macAddresses.contains (destMAC)   ? "SELF"
                                                                                     : destMAC == broadcastMAC ? "BROADCAST"
                                                                                                               : destMAC.toString());
    
    if (etherType == ETHERTYPE_IP)
    {
        outputString << " (IP)";
    }
    else if (etherType == ETHERTYPE_ARP)
    {
        outputString << " (ARP)";
    }
    else if (etherType == ETHERTYPE_REVARP)
    {
        outputString << " (RARP)";
    }
    else
    {
        outputString << " (UNKNOWN)";
    }
    
    outputString << newLine;
    
    return etherType;
}

void PacketParser::handleIP (const u_char* packet, bpf_u_int32 caplen, PacketData pData)
{
    const struct sniffIP *ip;
    int sizeIP;
    
    ip = (struct sniffIP*)(packet + SIZE_ETHERNET);
    sizeIP = IP_HL(ip) * 4;
    if (sizeIP < 20)
    {
        outputString << "Invalid IP header length: " << String (unsigned (sizeIP)) << " bytes"
                     << newLine;
        return;
    }
    
    IPAddress sourceIP (inet_ntoa (ip->IP_src));
    IPAddress destIP   (inet_ntoa (ip->IP_dst));
    
    bool selfSource = false;
    bool selfDest = false;
    bool broadcastDest = false;
    
    if (ipAddresses.contains (sourceIP))
        selfSource = true;
    
    if (ipAddresses.contains (destIP))
        selfDest = true;
    
    if (destIP == broadcastIP)
        broadcastDest = true;
    
    outputString << "Source IP: "    << (selfSource ? "SELF"
                                                    : sourceIP.toString())
                 << " Destination: " << (selfDest   ? "SELF"
                                                    : broadcastDest ? "BROADCAST"
                                                                    : destIP.toString());

    storeIPAddresses (sourceIP, destIP);
    
    pData.ttl = ip->IP_ttl;
    outputString << " TTL: " << pData.ttl
                 << newLine;
    
    switch (ip->IP_p) {
        case IPPROTO_TCP:
        {
            TCPConnection tcpCon;
            tcpCon.sourceIP = sourceIP;
            tcpCon.destIP = destIP;
            
            ++tcpCount;
            handleTCP (ip, packet, tcpCon, pData);
        }return;
        case IPPROTO_UDP:
        {
            ++udpCount;
            handleUDP (ip, packet, caplen, pData);
        }return;
        case IPPROTO_ICMP:
        {
            ++icmpCount;
            handleICMP (ip, packet, caplen, pData);
        }return;
        default:
        {
            outputString << "Protocol unknown"
                         << newLine;
        }return;
    }
}

void PacketParser::handleARP (const u_char* packet, PacketData pData)
{
    const struct sniffARP *arp;
    arp = (struct sniffARP*)(packet + SIZE_ETHERNET);
    
    pData.packetType = ARPPACKET;
    pData.ttl = -1;
    pData.payloadSize = -1;
    
    outputString << "Hardware type: "
                 << ((ntohs (arp->ARP_hType) == 1) ? "Ethernet"
                                                   : "Unknown")
                 << newLine;
    
    outputString << "Protocol: "
                 << ((ntohs (arp->ARP_pType) == 0x0800) ? "IPv4"
                                                        : "Unknown")
                 << newLine;
    
    if (ntohs (arp->ARP_hType) == 1 && ntohs (arp->ARP_pType) == 0x0800)// if ethernet and ipv4
    {
        IPAddress sourceIP    ((u_char*)arp + 14); // arp struct wasn't working
        IPAddress destIP      ((u_char*)arp + 24);
        MACAddress sourceMAC  (arp->ARP_sHa);
        MACAddress destMAC    (arp->ARP_tHa);
        int operation = ntohs (arp->ARP_oper);
        
        if (operation == ARP_REQUEST)
        {
            if (sourceIP == destIP && destMAC == broadcastMAC)
            {
                outputString << "Gratuitous ARP for " << sourceIP.toString() << " (Request)"
                             << newLine;
            }
            else
            {
                outputString << "Who is " << destIP.toString() << "? Tell " << sourceIP.toString()
                << newLine;
                
            }
        }
        else if (operation == ARP_REPLY)
        {
            outputString << sourceIP.toString() << " is at " << sourceMAC.toString()
                         << newLine;
        }
        else if (operation == RARP_REQUEST)
        {
            outputString << "Who is " << sourceMAC.toString() << "? Tell" << sourceMAC.toString()
                         << newLine;
        }
        else if (operation == RARP_REPLY)
        {
            outputString << destIP.toString() << "is at " << destMAC.toString()
                         << newLine;
        }
        
        pData.ARPOp = operation;
    }

    routeData (ARPPACKET, pData);
}

void PacketParser::handleTCP (const struct sniffIP *ip, const u_char* packet, TCPConnection tcpCon, PacketData pData)
{
    const struct sniffTCP *tcp;
    int sizeTCP;
    int sizeIP;
    const u_char* payload;
    
    pData.packetType = TCPPACKET;
    
    outputString << "(TCP)";
    
    sizeIP = IP_HL(ip) * 4;
    tcp = (struct sniffTCP*)(packet + SIZE_ETHERNET + sizeIP);
    sizeTCP = TCP_OFF(tcp) * 4;
    if (sizeTCP < 20)
    {
        outputString << "Invalid TCP header length: "
                     << String (unsigned (sizeTCP)) << " bytes"
                     << newLine;
        return;
    }
    
    tcpCon.sourcePort = ntohs (tcp->TCP_sPort);
    tcpCon.destPort = ntohs (tcp->TCP_dPort);
    
    int sIndex = tcpPortNumbers.indexOf (tcpCon.sourcePort);
    int dIndex = tcpPortNumbers.indexOf (tcpCon.destPort);
    
    outputString << " Source port: " << tcpCon.sourcePort;
    if (sIndex != -1)
    {
        outputString << " (" << tcpPortNames.getUnchecked (sIndex).toUpperCase() << ")";
    }
    
    outputString << " destination: " << tcpCon.destPort;
    if (dIndex != -1)
    {
        outputString << " (" << tcpPortNames.getUnchecked (dIndex).toUpperCase() << ")";
    }
    
    outputString << newLine;
    
//======================================FLAGS===========================================
    bool active = false;
    bool client = true;
    int index = -1;
    TCPConnection connection;
    zerostruct (connection);
    
    for (int i = 0; i < currentTCPConnections.size(); ++i)
    {
        connection = currentTCPConnections.get (i);
        if (tcpCon == connection) // connection will be client source
        {
            active = true;
            index = i;
            break;
        }
        else if (tcpCon.isInverse (connection))
        {
            active = true;
            client = false;
            index = i;
            break;
        }
    }
    
    if (!active)
    {
        if (tcp->TCP_flags & TH_SYN) // client sending SYN
        {
            tcpCon.currentStage = 1;
            tcpCon.ID = tcpCon.sourcePort + tcpCon.destPort + random.nextInt (1000);
            currentTCPConnections.insertAtHead (tcpCon);
        }
    }
    else
    {
        if ((connection.currentStage == 1) && (tcp->TCP_flags & TH_SYN) && !client) // server sending SYN
        {
            ++connection.currentStage;
            currentTCPConnections.set (index, connection);
        }
        else if (connection.currentStage == 2 && (tcp->TCP_flags & TH_ACK) && client) // client sending ACK
        {
            ++connection.currentStage;
            currentTCPConnections.set (index, connection);
            pData.TCPConnection = true;
            startTimer (connection.ID, 30000);
        }
        else if (connection.currentStage == 3)
        {
            if (tcp->TCP_flags & TH_FIN) // one side initiates connection close
            {
                ++connection.currentStage;
                currentTCPConnections.set (index, connection);
                startTimer (connection.ID, 5000); // timeout timer - deals with half-closed connections
            }
            else
            {
                startTimer (connection.ID, 30000);
                pData.TCPConnectionPacket = true;
            }
        }
        else if ((connection.currentStage == 4) && (tcp->TCP_flags & TH_FIN) && !client) // other side closes - graceful close
        {
            ++connection.currentStage;
            currentTCPConnections.remove (index);
            pData.TCPDisconnection = true;
        }
    }
    
//======================================PAYLOAD===========================================
    payload = (u_char*)(packet + SIZE_ETHERNET + sizeIP + sizeTCP);
    pData.payloadSize = ntohs(ip->IP_len) - (sizeIP + sizeTCP);
    
    if (pData.payloadSize == 0)
    {
        outputString << "No payload"
                     << newLine;
    }
    else
    {
        outputString << "Payload (" << pData.payloadSize << " bytes)"
        << newLine;
        
        if (pData.payloadSize < 500)
            printPayload (payload, pData.payloadSize);
    }
    
    pData.TCPID = connection.ID;
    routeData (TCPPACKET, pData);
}

void PacketParser::handleUDP (const struct sniffIP *ip, const u_char* packet, bpf_int32 packetLen, PacketData pData)
{
    const struct sniffUDP *udp;
    int sizeUDP;
    int sizeIP;
    const u_char* payload;
    
    pData.packetType = UDPPACKET;
    
    outputString << "(UDP)";
    
    sizeIP = IP_HL(ip) * 4;
    sizeUDP = packetLen - SIZE_ETHERNET - sizeIP;
    if (sizeUDP < sizeof (struct sniffUDP))
    {
        outputString << "Invalid UDP header length "
                     << String (unsigned (sizeUDP)) << " bytes"
                     << newLine;
        return;
    }
    
    udp = (struct sniffUDP*)(packet + SIZE_ETHERNET + sizeIP);
    
    int sourcePort = ntohs (udp->UDP_sPort);
    int destPort = ntohs (udp->UDP_dPort);
    
    int sIndex = udpPortNumbers.indexOf (sourcePort);
    int dIndex = udpPortNumbers.indexOf (destPort);
    
    outputString << " Source port: " << sourcePort;
    if (sIndex != -1)
    {
        outputString << " (" << udpPortNames.getUnchecked (sIndex).toUpperCase() << ")";
    }
    
    outputString << " destination: " << destPort;
    if (dIndex != -1)
    {
        outputString << " (" << udpPortNames.getUnchecked (dIndex).toUpperCase() << ")";
    }
    
    outputString << newLine;

//======================================PAYLOAD===========================================
    payload = (u_char*)(packet + SIZE_ETHERNET + sizeIP + 8);
    pData.payloadSize = ntohs (ip->IP_len) - (sizeIP + 8);
    
    if (pData.payloadSize == 0)
    {
        outputString << "No payload"
        << newLine;
    }
    else
    {
        outputString << "Payload (" << pData.payloadSize << " bytes):"
        << newLine;
        
        if (pData.payloadSize < 500)
            printPayload (payload, pData.payloadSize);
    }
    
    routeData (UDPPACKET, pData);
}

void PacketParser::handleICMP (const struct sniffIP *ip, const u_char* packet, bpf_int32 packetLen, PacketData pData)
{
    const struct sniffICMP *icmp;
    int sizeICMP;
    int sizeIP;
    
    pData.packetType = ICMPPACKET;
    
    outputString << "(ICMP)";
    
    sizeIP = IP_HL(ip) * 4;
    sizeICMP = packetLen - SIZE_ETHERNET - sizeIP;
    if (sizeICMP < sizeof(struct sniffUDP))
    {
        outputString << "Invalid ICMP header length "
                     << String (unsigned (sizeICMP)) << " bytes"
                     << newLine;
        return;
    }
    
    icmp = (struct sniffICMP*)(packet + SIZE_ETHERNET + sizeIP);
    
    switch (icmp->ICMP_type) {
        case 0:
            outputString << " Type: Echo Reply"
                         << newLine;
            break;
        case 3:
            outputString << " Type: Destination Unreachable"
                         << newLine;
            break;
        case 5:
            outputString << " Type: Redirect Message"
                         << newLine;
            break;
        case 8:
            outputString << " Type: Echo Request"
                         << newLine;
            break;
        case 9:
            outputString << " Type: Router Advertisement"
                         << newLine;
            break;
        case 10:
            outputString << " Type: Router Solicitation"
                         << newLine;
            break;
        case 11:
            outputString << " Type: Time Exceeded"
                         << newLine;
            break;
        default:
            outputString << " Type: Other"
                         << newLine;
            break;
    }
    
    outputString << "Code: " << icmp->ICMP_code
                 << newLine;
    
    pData.payloadSize = ntohs (ip->IP_len) - (sizeIP + 8);
    
    routeData (ICMPPACKET, pData);
}

void PacketParser::printPayload (const u_char* payload, int len)
{
    int lenRem = len;
    int lineWidth = 16;
    int lineLen;
    int offset = 0;
    const u_char *ch = payload;
    
    if (len <= 0)
        return;
    
    if (len <= lineWidth)
    {
        printHexAsciiLine (ch, len, offset);
        return;
    }
    
    for( ;; ) {
        // current line length
        lineLen = lineWidth % lenRem;
        
        // print line
        printHexAsciiLine (ch, lineLen, offset);
        
        // work out total remaining
        lenRem = lenRem - lineLen;
        
        // shift pointer to remaining bytes to print
        ch = ch + lineLen;
        
        // add offset
        offset = offset + lineWidth;
        
        // check if lineWidth chars or less
        if (lenRem <= lineWidth)
        {
            printHexAsciiLine (ch, lenRem, offset);
            break;
        }
    }
    
    return;
}

void PacketParser::printHexAsciiLine (const u_char *payload, int len, int offset)
{
    int gap;
    const u_char *ch;
    
    // offset
    outputString << String::formatted ("%05d    ", offset);
    
    // hex
    ch = payload;
    for (int i = 0; i < len; ++i)
    {
        outputString << String::formatted ("%02x ", *ch);
        
        ch++;
        if (i == 7)
            outputString << " ";
    }
    if (len < 8)
        outputString << " ";
    
    if (len < 16)
    {
        gap = 16 - len;
        for (int i = 0; i < gap; i++)
        {
            outputString << String ("   ");
        }
    }
    outputString << "   ";
    
    // ASCII (if printable)
    ch = payload;
    for (int i = 0; i < len; ++i)
    {
        if (isprint (*ch))
            outputString << String::formatted ("%c", *ch);
        else
            outputString << String (".");
        ch++;
    }
    outputString << newLine;
    
    return;
}

void PacketParser::storeIPAddresses (IPAddress source, IPAddress dest)
{
    bool contains = false;
    for (int i = 0; i < sourceIPs.size(); ++i)
    {
        if (sourceIPs[i].address == source)
        {
            IP temp;
            temp.address = source;
            temp.freq = sourceIPs[i].freq + 1;
            sourceIPs.set (i, temp);
            contains = true;
            break;
        }
    }
    if (!contains)
    {
        IP newIP;
        newIP.address = source;
        newIP.freq = 1;
        sourceIPs.add (newIP);
    }
    
    contains = false;
    for (int i = 0; i < destIPs.size(); ++i)
    {
        if (destIPs[i].address == dest)
        {
            IP temp;
            temp.address = dest;
            temp.freq = destIPs[i].freq + 1;
            destIPs.set (i, temp);
            contains = true;
            break;
        }
    }
    if (!contains)
    {
        IP newIP;
        newIP.address = dest;
        newIP.freq = 1;
        destIPs.add (newIP);
    }
    
    // store unique IP connections
    IPConnection connection;
    connection.source = source;
    connection.destination = dest;
    
    sharedMem.enter();
    contains = false;
    for (int i = 0; i < IPConnections.size(); ++i)
    {
        if (connection == IPConnections.get (i))
        {
            contains = true;
            break;
        }
    }
    if (!contains)
    {
        IPConnections.insertAtHead (connection);
    }
    sharedMem.exit();
}

void PacketParser::routeData(int protocol, PacketData dataToSend)
{
    DataToBeMapped data;
    data.t = DataToBeMapped::Packet;
    
    switch (protocol) {
        case ARPPACKET:
            data.identifier = DataIdentifiers::ARP;
            break;
        case TCPPACKET:
            data.identifier = DataIdentifiers::TCP;
            break;
        case UDPPACKET:
            data.identifier = DataIdentifiers::UDP;
            break;
        case ICMPPACKET:
            data.identifier = DataIdentifiers::ICMP;
            break;
    }
        
    data.pData = dataToSend;
    audio.networkDataQueue.addToFifo (&data, 1);
    
    if (graphicsComp.isActive() && !graphicTriggerLocks[protocol])
    {
        packetToVisualise[protocol] = dataToSend;
        (new UpdateGraphicsCallback (this))->post();
        graphicTriggerLocks[protocol] = true;
        startTimer (protocol, sampleInterval);
    }
    
    if (listener != nullptr)
    {
        switch (protocol) {
            case ARPPACKET:
                listener->newPacket (protocol, arpCount);
                break;
            case TCPPACKET:
                listener->newPacket (protocol, tcpCount);
                break;
            case UDPPACKET:
                listener->newPacket (protocol, udpCount);
                break;
            case ICMPPACKET:
                listener->newPacket (protocol, icmpCount);
                break;
            default:
                break;
        }
    }
    
}

void PacketParser::zeroCounters()
{
    arpCount = 0;
    tcpCount = 0;
    udpCount = 0;
    icmpCount = 0;
    arpLastCount = 0;
    tcpLastCount = 0;
    udpLastCount = 0;
    icmpLastCount = 0;
    arpPps = 0;
    tcpPps = 0;
    udpPps = 0;
    icmpPps = 0;
}

void PacketParser::updateGraphics()
{
    for (int i = 0; i < 4; ++i)
    {
        if (packetToVisualise[i].packetType != -1)
        {
            graphicsComp.newPacket (packetToVisualise[i]);
            packetToVisualise[i].packetType = -1;
        }
    }
}

void PacketParser::printString (String str)
{
    textComp.addToQueue (str);
}