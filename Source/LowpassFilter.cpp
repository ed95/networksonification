/*
  ==============================================================================

    LowpassFilter.cpp
    Created: 18 Dec 2015 1:54:34pm
    Author:  Edward Davies

  ==============================================================================
*/

#include "LowpassFilter.h"

LowpassFilter::LowpassFilter (Audio& audio_) : BaseFilter (audio_)
{
    
}

LowpassFilter::~LowpassFilter()
{
    
}

void LowpassFilter::setCutoff(float frequency)
{
    float qVal = getQ();
    float kVal = tanf (M_PI * frequency / getSampleRate());
    float norm = 1.f / (1.f + kVal / getQ() + kVal * kVal);
    
    float b0 = kVal * kVal * norm;
    
    setCoeffs (b0, 2 * b0, b0, 2 * (kVal * kVal - 1) * norm, (1 - kVal / qVal + kVal * kVal) * norm);
}