/*
  ==============================================================================

    AudioControls.h
    Created: 18 Dec 2015 2:29:53pm
    Author:  Edward Davies

  ==============================================================================
*/

#ifndef AUDIOCONTROLS_H_INCLUDED
#define AUDIOCONTROLS_H_INCLUDED

#include "stdio.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"

/**
    UI containing audio controls
 */
class AudioControls : public Component,
                      public Slider::Listener
{
public:
    /**
        Constructor.
        @param audio_ Reference to the audio class @see Audio
     */
    AudioControls (Audio& audio_);
    
    /**
        Destructor.
     */
    ~AudioControls();
    
    /**
        Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
         Inherited from Component.
     */
    void resized() override;
    
    /**
        Inherited from SliderListener.
     */
    void sliderValueChanged (Slider* slider) override;
    
private:
    Audio& audio; /// Reference to the Audio class
    
    Slider masterLevelSlider; /// Slider to control the master level
    
    Image volImage; /// Image for the volume slider
    ImageComponent volImageComponent; /// Component to hold the volume slider image
};

#endif  // AUDIOCONTROLS_H_INCLUDED
