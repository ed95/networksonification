//
//  AudioParameters.h
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#ifndef AUDIOPARAMETERS_H_INCLUDED
#define AUDIOPARAMETERS_H_INCLUDED
#include "Network.h"

/**
    Enum for audio parameters.
 */
enum Parameters
{
    pSampleRate = 0,
    pMasterLevel,
    pAAttack0,
    pAAttack1,
    pAAttack2,
    pAAttack3,
    pADecay,
    pASustain,
    pARelease0,
    pARelease1,
    pARelease2,
    pARelease3,
    pFAttack,
    pFDecay,
    pFRelease,
    pFSustain,
    pFilterQ,
    pOscillatorLevel0,
    pOscillatorLevel1,
    pOscillatorLevel2,
    pOscillatorLevel3,
    pReverbRoomSize,
    pReverbWidth,
    pFilterCutoff,
    pFilterType,
    pARPChannel,
    pTCPChannel,
    pUDPChannel,
    pICMPChannel,
    pStopAllNotes,
    pResetPayloadSize,
    numParameters
};

/**
    Enum for network data.
 */
enum DataIdentifiers
{
    ARP = 0,
    TCP,
    UDP,
    ICMP,
    packetsPerSecond,
    ppsARP,
    ppsTCP,
    ppsUDP,
    ppsICMP,
    numIPConnections
};

/**
    Struct to store audio parameters before adding to the audio queue.
 */
struct ParameterPair {
    Parameters identifier;
    
    enum Type {
        Int,
        Float,
        Bool,
        Flag
    };
    Type t;
    
    union {
        float fVal;
        int iVal;
        bool bVal;
    };
};

/**
    Struct to store network data before adding to the network queue.
 */
struct DataToBeMapped {
    DataIdentifiers identifier;
    
    enum Type {
        Int,
        Float,
        Packet
    };
    Type t;
    
    union {
        float fVal;
        int iVal;
        PacketData pData;
    };
};

/**
    Floating-point parameter class.
 */
class FloatParameter
{
public:
    /**
        Constructor.
        @param defaultParameterValue The default value for this parameter
        @param paramName The name of this parameter
        @param coeff The smoothing coefficient for this parameter (0 = lots of smoothing, 1 = no smoothing)
     */
    FloatParameter (float defaultParameterValue, Parameters paramName, float coeff)
    : defaultValue (defaultParameterValue), value (defaultParameterValue), name (paramName), a (coeff), b (1 - a), previous (defaultParameterValue)
    {
        
    }
    
    /**
        Returns the smoothed value.
        @return The smoothed value based on the smoothing coefficient.
     */
    float getValue()
    {
        return previous = (a * value) + (b * previous);
    }
    
    /**
        Sets this parameter's value.
        @param newValue The new value to set this parameter to
     */
    void setValue (float newValue)
    {
        value = newValue;
    }
    
    /**
        Returns the default value of this parameter.
        @return The default value that was supplied in the constructor
     */
    float getDefaultValue() const
    {
        return defaultValue;
    }
    
    /**
        Resets this parameter.
     */
    void reset()
    {
        previous = 0;
    }
    
    /**
        Gets the name of this parameter.
        @return The enum that was supplied in the constructor
     */
    Parameters getIdentifier()
    {
        return name;
    }
    
private:
    float defaultValue; /// The default value of this parameter
    float value;        /// The current value of this parameter
    Parameters name;    /// The enum of this parameter
    float a;            /// Smoothing weighting for current value
    float b;            /// Smoothing weighting for previous value
    float previous;     /// The previous value
};

const float defaultSampleRate = 44100.f;

const float defaultMasterLevel = 1.f;
const float defaultAmpAttack = 0.1;
const float defaultAmpDecay = 0.1;
const float defaultAmpSustain = 1.f;
const float defaultAmpRelease = 0.5;

const float defaultFilterAttack = 0.3;
const float defaultFilterDecay = 0.3;
const float defaultFilterSustain = 1.f;
const float defaultFilterRelease = 0.1;
const float defaultFilterQ = 0.7;

const float defaultOscillatorLevel = 1.f;

const float defaultReverbRoomSize = 0.f;
const float defaultReverbWidth = 0.f;

/**
     Integer parameter class.
 */
class IntegerParameter
{
public:
    /**
        Constructor.
        @param defaultParameterValue The default value for this parameter
        @param minValue The minimum value to clip this parameter at
        @param minValue The maximum value to clip this parameter at
        @param paramName The name of this parameter
        @param coeff The smoothing coefficient for this parameter (0 = lots of smoothing, 1 = no smoothing)
     */
    IntegerParameter (int defaultParameterValue, int minValue, int maxValue, Parameters paramName, float coeff) : defaultValue (defaultParameterValue), value (defaultParameterValue), min (minValue), max (maxValue), name (paramName), a (coeff), b (1 - a), previous (defaultParameterValue)
    {
        
    }
    
    /**
         Returns the smoothed value between minValue and maxValue
         @return The smoothed value based on the smoothing coefficient.
     */
    int getValue()
    {
        if (value > max)
            return max;
        else if (value < min)
            return min;
        
        return previous = (a * value) + (b * previous);
    }
    
    /**
         Sets this parameter's value.
         @param newValue The new value to set this parameter to
     */
    void setValue (int newValue)
    {
        value = newValue;
    }
    
    /**
         Returns the default value of this parameter.
         @return The default value that was supplied in the constructor
     */
    int getDefaultValue() const
    {
        return defaultValue;
    }
    
    /**
         Gets the name of this parameter.
         @return The enum that was supplied in the constructor
     */
    Parameters getIdentifier ()
    {
        return name;
    }
    
private:
    int defaultValue; /// The default value of this parameter
    int value;        /// The current value of this parameter
    int min;          /// The minimum value of this parameter
    int max;          /// The maximum value of this parameter
    Parameters name;  /// The enum of this parameter
    float a;          /// Smoothing weighting for current value
    float b;          /// Smoothing weighting for previous value
    int previous;     /// The previous value
};

const int defaultFilterCutoff = 20000;
const int defaultFilterType = 0;
const int defaultARPChannel = 0;
const int defaultTCPChannel = 1;
const int defaultUDPChannel = 2;
const int defaultICMPChannel = 3;

/**
     Boolean parameter class.
 */
class BooleanParameter
{
public:
    /**
         Constructor.
         @param defaultParameterValue The default value for this parameter
         @param paramName The name of this parameter
     */
    BooleanParameter (bool defaultParameterValue, Parameters paramName)
    : defaultValue (defaultParameterValue),
    value (defaultParameterValue),
    name (paramName)
    {
        
    }
    
    /**
         Returns the value of the parameter.
         @return The smoothed value based on the smoothing coefficient.
     */
    bool getValue() const
    {
        return value;
    }
    
    /**
         Sets this parameter's value.
         @param newValue The new value to set this parameter to
     */
    void setValue (bool newValue)
    {
        value = newValue;
    }
    
    /**
        Returns the default value of this parameter.
        @return The default value that was supplied in the constructor
    */
    float getDefaultValue() const
    {
        return defaultValue;
    }
    
    /**
         Gets the name of this parameter.
         @return The enum that was supplied in the constructor
     */
    Parameters getIdentifier ()
    {
        return name;
    }
    
private:
    bool defaultValue; /// The default value of this parameter
    bool value;        /// The current value of this parameter
    Parameters name;   /// The enum of this parameter
};

#endif  // AUDIOPARAMETERS_H_INCLUDED
