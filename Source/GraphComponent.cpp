//
//  GraphComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 30/03/2016.
//
//

#include "GraphComponent.hpp"

GraphComponent::GraphComponent (PacketGraphicsComponent& graphicsComp_) : graphicsComp (graphicsComp_), PPSLabel ("", "PPS"), timeLabel ("", "Time")
{
    for (int i = 0; i < 4; ++i)
        lineColours[i] = graphicsComp.getColourAtIndex (i);
    
    graphicsComp.setListener (this);
    
    PPSLabel.setJustificationType (Justification::centred);
    timeLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (&PPSLabel);
    addAndMakeVisible (&timeLabel);
    
    int val = 0;
    for (int i = 0; i < 4; ++i)
    {
        PPSVals[i].setJustificationType (Justification::centred);
        PPSVals[i].setText (String (val), dontSendNotification);
        val += 50;
        
        addAndMakeVisible (PPSVals[i]);
    }
    
    isVisible = false;
    snifferState = false;
    state = false;
}

GraphComponent::~GraphComponent()
{
}

void GraphComponent::paint (Graphics& g)
{    
    for (int i = 0; i < 4; ++i)
    {
        Path p;
        p.startNewSubPath (xPos[0], yPos[i][0]);
        
        for (int j = 1; j < yPos[i].size(); ++j)
            p.lineTo (xPos[j], yPos[i][j]);
        
        g.setColour (lineColours[i]);
        g.strokePath (p, PathStrokeType (1.f));
    }
    
    g.setColour (Colours::black);
    g.fillRect (graphBounds.getBottomLeft().getX(), graphBounds.getBottomLeft().getY(), graphBounds.getWidth(), 1);
    g.fillRect (graphBounds.getTopLeft().getX(), graphBounds.getTopLeft().getY(), 1, graphBounds.getHeight());
}

void GraphComponent::resized()
{
    graphBounds = Rectangle<int> (40, 10, getWidth() - 50, getHeight() - 50);
    int width = graphBounds.getWidth();
    
    stepSize = 1;
    numSteps = width / stepSize;
    
    for (int i = 0; i < numSteps; ++i)
        xPos.add (graphBounds.getX() + (i * stepSize));
    
    for (int i = 0; i < 4; ++i)
    {
        lastVal[i] = 0;
        yPos[i].add (mapToCoordinate (lastVal[i]));
    }
    
    repaint();
    
    timeLabel.setBounds (0, graphBounds.getBottom(), getWidth(), 40);
    PPSLabel.setBounds (0, graphBounds.getBottom(), 40, 40);
    
    for (int i = 0; i < 4; ++i)
        PPSVals[i].setBounds (0, graphBounds.getBottom() - 20 - (i * (graphBounds.getHeight() * .33)), 40, 40);
}

void GraphComponent::timerCallback()
{
    for (int i = 0; i < 4; ++i)
    {
        if (yPos[i].size() >= numSteps)
            yPos[i].remove (0);
        
        yPos[i].add (mapToCoordinate (lastVal[i]));
    }
    
    repaint();
}

void GraphComponent::newGraphData (PPSData pps)
{
    lastVal[0] = pps.arpPPS;
    lastVal[1] = pps.tcpPPS;
    lastVal[2] = pps.udpPPS;
    lastVal[3] = pps.icmpPPS;
}

void GraphComponent::colourChanged(int protocol, juce::Colour newColour)
{
    lineColours[protocol] = newColour;
}

void GraphComponent::isSelected (bool selected)
{
    isVisible = selected;
    state  = (isVisible && snifferState);
    
    state ? startTimer (10) : stopTimer();
}

void GraphComponent::isSnifferRunning (bool running)
{
    snifferState = running;
    
    if (running)
    {
        for (int i = 0; i < 4; ++i)
        {
            lastVal[i] = 0;
            yPos[i].clear();
            yPos[i].add (mapToCoordinate (lastVal[i]));
        }
    }
    
    state = (snifferState && isVisible);
    
    state ? startTimer (10) : stopTimer();
}

int GraphComponent::mapToCoordinate (float ppsVal)
{
    int height = graphBounds.getBottom();
    
    return height - (jmin ((ppsVal / 150.f), 1.f) * height);
}