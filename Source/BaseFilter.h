/*
  ==============================================================================

    BaseFilter.h
    Created: 18 Dec 2015 1:54:22pm
    Author:  Edward Davies

  ==============================================================================
*/

#ifndef BASEFILTER_H_INCLUDED
#define BASEFILTER_H_INCLUDED

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"

/**
    A base filter class that other filters can inherit from
 */
class BaseFilter
{
public:
    /**
        Constructor. 
        @param audio_ A reference to the Audio class. @see Audio
     */
    BaseFilter (Audio& audio_);
    
    /**
        Destructor.
     */
    virtual ~BaseFilter();
    
    /**
        Returns the Q value of the filter.
        @return The Q value.
     */
    float getQ();
    
    /**
        Returns the current sample rate.
        @return The current sample rate.
     */
    float getSampleRate();
    
    /**
        Sets the cutoff based on the filter coefficients. Classes that inherit from BaseFilter should override this method.
        @param frequency The cutoff frequency.
     */
    virtual void setCutoff (float frequency) = 0;
    
    /**
        Sets the coefficients of the BiQuad filter.
        @param b0 Coefficient 1
        @param b1 Coefficient 2
        @param b2 Coefficient 3
        @param a1 Coefficient 4
        @param a2 Coefficient 5
     */
    void setCoeffs (float b0, float b1, float b2, float a1, float a2);
    
    /**
        Processes an input sample and returns the filtered sample value.
        @param inputSample The sample to be filtered
        @return The filtered input sample
     */
    float outputFiltered (float inputSample);
    
private:
    stk::BiQuad filter; /// STK BiQuad filter object
    Audio& audio; /// Reference to the Audio class
    float qVal; /// The Q value of the filter
};


#endif  // BASEFILTER_H_INCLUDED
