//
//  GraphComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 30/03/2016.
//
//

#ifndef GraphComponent_hpp
#define GraphComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PacketParser.hpp"

/**
    UI component for graphing packets per second data for each protocol
 */
class GraphComponent : public Component,
                       public Timer,
                       public PacketGraphicsComponent::Listener
{
public:
    /**
        Constructor.
        @param Reference to the graphics component
     */
    GraphComponent (PacketGraphicsComponent& graphicsComp_);
    
    /**
        Destructor.
     */
    ~GraphComponent();
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
        Inherited from Component.
     */
    void resized() override;
    
    /**
        Inherited from Timer.
     */
    void timerCallback() override;
    
    /**
        Called when new PPS data is calculated, sets the Y-pos of the lines.
        @param pps The packets per second data struct
     */
    void newGraphData (PPSData pps);
    
    /**
        Called when a packet protocol colour is changed in the graphics settings.
        @param protocol The protocol that has changed colour
        @param newColour The new colour for the protocol
     */
    void colourChanged (int protocol, Colour newColour) override;
    
    /**
        Sets whether the graph component is on screen - called when the main app tab is selected/deselected.
        @param selected True if the graph is visible, false if not
     */
    void isSelected (bool selected);
    
    /**
         Tells the graph if the packet sniffer is running.
         @param running True if the sniffer is running, false if not
     */
    void isSnifferRunning (bool running);
    
    /**
        Returns the current state of the graph.
     */
    bool getState() { return state; };
    
    /**
        Maps a PPS value to a y-coordinate on the screen,
        @param ppsVal The PPS value
        @return The y-pos
     */
    int mapToCoordinate (float ppsVal);
    
private:
    PacketGraphicsComponent& graphicsComp; /// Reference to the graphics component for listener
    
    Array<int> xPos;            /// The X co-ordinate for each step
    Array<float> yPos[4];       /// The Y co-ordinate for each protcol for each step
    float lastVal[4];           /// The previous PPS value for each protocol
    Rectangle<int> graphBounds; /// The graph bounds
    
    int stepSize; /// How fast to move along the X-axis
    int numSteps; /// How many step sizes fit in the graph width
    
    Colour lineColours[4]; /// Colour for each protocol - used to draw the line
    
    Label PPSLabel; /// Y-axis label
    Label timeLabel; /// X-axis label
    
    Label PPSVals[4]; /// PPS values for each protocol
    
    bool isVisible; /// True if graph is visible on the screen
    bool snifferState; /// True if the sniffer is active
    bool state; /// The combine state
    
    bool drawGraphOutline; /// Draws the graph outline if true
};

#endif /* GraphComponent_hpp */
