//
//  SquareOscillator.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 16/12/2015.
//
//

#include "SquareOscillator.hpp"

SquareVoice::SquareVoice (Audio& audio_) : BaseOscillator (audio_, 3)
{
    isVibratoOn = false;
}

SquareVoice::~SquareVoice()
{
    
}

bool SquareVoice::canPlaySound (SynthesiserSound *sound)
{
    return dynamic_cast<SquareSound*> (sound) != nullptr;
}

void SquareVoice::setSTKFreq (float frequency)
{
    squareGen.setFrequency (frequency);
    baseFreq = frequency;
}

void SquareVoice::resetSTKGen()
{
    squareGen.reset();
    vibGen.reset();
}

float SquareVoice::renderWaveshape()
{
    if (isVibratoOn)
    {
        float relativeDepth = vDepth * baseFreq;
        squareGen.setFrequency (baseFreq + (vibGen.tick() * relativeDepth));
        return squareGen.tick();
    }
    else
    {
        return squareGen.tick();
    }
}

void SquareVoice::vibratoOn (bool isOn)
{
    isVibratoOn = isOn;
}

void SquareVoice::setVibRateDepth (float vibRate, float vibDepth)
{
    vibGen.setFrequency (vibRate);
    vDepth = vibDepth;
}