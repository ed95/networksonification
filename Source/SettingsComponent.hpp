//
//  SettingsComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/03/2016.
//
//

#ifndef SettingsComponent_hpp
#define SettingsComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "NetworkSettingsComponent.hpp"
#include "GraphicsSettingsComponent.hpp"
#include "Audio.hpp"

/**
    Component for the settings page
 */
class SettingsComponent : public Component
{
public:
    /**
        Constructor.
        @param audio_ Reference to the Audio class @see Audio
        @param packetSniffer_ Reference to the PacketSniffer class @see PacketSniffer
        @param graphicsComp_ Reference to the PacketGraphicsComponent class @see PacketGraphicsComponent
     */
    SettingsComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketGraphicsComponent& graphicsComp_);
    
    /**
        Destructor.
     */
    ~SettingsComponent();
    
    /**
         Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
        Called when settings page becomes active - remembers the network settings for comparison later.
     */
    void rememberCurrentNetworkSettings();
    
private:
    Label headerLabel;   /// Label for top of settings page
    Label audioLabel;    /// Label for audio settings
    Label networkLabel;  /// Label for network settings
    Label graphicsLabel; /// Label for graphics settings
    
    int itemHeight; /// Height of UI items
    
    AudioDeviceSelectorComponent audioSettingsComp; /// Audio settings component
    NetworkSettingsComponent networkSettingsComp;   /// Network settings component
    GraphicsSettingsComponent graphicsSettingsComp; /// Graphics settings component
};

#endif /* SettingsComponent_hpp */
