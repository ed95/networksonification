//
//  PacketUI.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#ifndef PacketUI_hpp
#define PacketUI_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "GraphComponent.hpp"
#include "PacketSniffer.hpp"

/**
    UI component for packet capture.
 */
class PacketUI : public Component,
                 public Button::Listener,
                 public PacketParser::Listener,
                 public ComboBox::Listener,
                 public MultiTimer
{
public:
    /**
        Constructor.
        @param audio_ Reference to the Audio class @see Audio
        @param packetSniffer_ Reference to the PacketSniffer class @see PacketSniffer
        @param packetParser_ Reference to the PacketParser class @see PacketParser
        @param protocolSliders_ Reference to the ProtocolSliders class @see ProtocolSliders
     */
    PacketUI (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_, GraphComponent& graphComp_);
    
    /**
        Destructor.
     */
    ~PacketUI();
    
    /**
        Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
         Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from ButtonListener.
     */
    void buttonClicked (Button* button) override;
    
    /**
        Inherited from PacketParser::Listener. Called by PacketParser when new PPS data
        @param pps The packets per second data
     */
    void updateLabels (PPSData pps) override;
    
    /**
         Inherited from PacketParser::Listener. Called by PacketParser when new packet is captured
         @param packetProtocol The protocol of the packet
     */
    void newPacket (int packetProtocol, int packetCount) override;
    
    /**
         Inherited from ComboBoxListener
     */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    /**
        Inherited from MultiTimer.
     */
    void timerCallback (int timerID) override;
    
private:
    Audio& audio;                     /// Reference to the Audio class
    PacketSniffer& packetSniffer;     /// Reference to the PacketSniffer class
    PacketParser& packetParser;       /// Reference to the PacketParser class
    GraphComponent& graphComp;        /// Reference to the GraphComponent class
    
    Label arpLabel, tcpLabel, udpLabel, icmpLabel;
    Label arpCounter, tcpCounter, udpCounter, icmpCounter;
    Label arpPpsLabel, tcpPpsLabel, udpPpsLabel, icmpPpsLabel;
    
    Image playImage; /// Image for the start button
    Image stopImage; /// Image for the stop button
    
    ImageButton startButton; /// Start button
    ImageButton stopButton;  /// Stop button
    
    ComboBox protocolWaveshape[4]; /// ComboBox to select which waveshape to use for each protocol
    
    int itemHeight; /// The item height for positioning
};
#endif /* PacketUI_hpp */
