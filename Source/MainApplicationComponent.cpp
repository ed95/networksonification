//
//  MainApplicationComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/03/2016.
//
//

#include "MainApplicationComponent.hpp"

MainApplicationComponent::MainApplicationComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_, PacketGraphicsComponent& graphicsComp_) : packetUI (audio_, packetSniffer_, packetParser_, graphComp), audioControls (audio_), graphComp (graphicsComp_)
{
    addAndMakeVisible (&packetUI);
    addAndMakeVisible (&audioControls);
    addAndMakeVisible (&graphComp);
}

MainApplicationComponent::~MainApplicationComponent()
{
    
}

void MainApplicationComponent::paint (Graphics& g)
{
    
}

void MainApplicationComponent::resized()
{
    Rectangle<int> r = Rectangle<int> (getBounds());
    
    packetUI.setBounds (r.removeFromTop (proportionOfHeight (0.3)).withX (0));
    graphComp.setBounds (r.removeFromTop (proportionOfHeight (0.6)).withX (0));
    audioControls.setBounds (r.removeFromTop (proportionOfHeight (0.1)).withX (0));
}

void MainApplicationComponent::setActive (bool isActive)
{
    graphComp.isSelected (isActive);
}