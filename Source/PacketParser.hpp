//
//  PacketParser.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 06/12/2015.
//
//

#ifndef PacketParser_hpp
#define PacketParser_hpp

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Network.h"
#include "Audio.hpp"
#include "PacketGraphicsComponent.hpp"
#include "TextComponent.hpp"

#define IPWINDOWID -1
#define IPWINDOWTIME 1000

#define PPSWINDOWID -2
#define PPSWINDOWTIME 200
#define SMOOTHINGFACTOR 0.5

#define IPPRINTID -3
#define IPPRINTTIME 1500

/**
    Array sorting class
 */
class ArraySorter
{
public:
    static int compareElements (IP a, IP b)
    {
        if (a.freq < b.freq)
            return 1;
        else if (a.freq > b.freq)
            return -1;
        else // if a == b
            return 0;
    }
};

/**
    Packet parser class that decodes the packet data received from the PacketSniffer class.
 */
class PacketParser : public MultiTimer,
                     public Component
{
public:
    /**
        Constructor.
        @param audio_
        @param graphicsComp_
        @param textComp_
     */
    PacketParser (Audio& audio_, PacketGraphicsComponent& graphicsComp_, TextComponent& textComp_);
    
    /**
        Destructor.
     */
    ~PacketParser();
    
    /**
        Called when a packet is captured on the network hardware device, parses the packet.
         @param packetCount The number of this packet since the session started
         @param pkthdr The generic pcap header
         @param packet Pointer to the packet data
     */
    void parsePacket (int packetCount, const struct pcap_pkthdr pkthdr, const u_char* packet);
    
    /**
         Called when sniffer fails to capture a packet - prints to the text display.
     */
    void didntGrab();
    
    /**
         Called when sniffer session ends - prints some stats about the session.
        @param pCount The number of packets captured during the session
        @param t The total time of the session
     */
    void sessionStats (int pCount, RelativeTime t);
    
    /**
        Prints the source and destination IP addresses captured during the session.
        @param sorted True if IP addresses should be sorted by frequency
     */
    void printIPs (bool sorted);
    
    /**
         Listener class.
     */
    class Listener
    {
    public:
        /** 
            Destructor.
         */
        virtual ~Listener(){}
        
        /**
            Called when a new packet is extracted.
            @param packetType The protocol of the packet
         */
        virtual void newPacket (int packetProtocol, int packetCount) = 0;
        
        virtual void updateLabels (PPSData pps) = 0;
    };
    
    /**
         Sets the listener.
        @param newListener The new listener
     */
    void setListener (Listener* newListener);
    
    /**
        Inherited from MultiTimer.
     */
    void timerCallback (int timerID) override;
    
    /**
        Called to update the graphics component.
     */
    void updateGraphics();
    
    /**
        Prints a string of packet data to the text output.
        @param str The string to print
     */
    void printString (String str);
    
    /**
        Sets the sample interval of the graphics display.
        @param ms The interval to use in ms
     */
    void setSampleInterval (int ms);
    
    /**
     Called by PacketSniffer when the packet capture session starts.
     */
    void captureStarted();
    
    /**
        Called by PacketSniffer when the packet capture session stops.
     */
    void captureStopped();
    
private:
    /**
        Decodes an Ethernet frame.
        @param packet Pointer to the packet data
        @return The packet protcol
     */
    u_int16_t handleEthernet (const u_char* packet);
    
    /**
        Decodes an IP datagram.
        @param packet Pointer to the packet data
        @param caplen The length of the captured packet
     */
    void handleIP (const u_char* packet, bpf_u_int32 caplen, PacketData pData);
    
    /**
         Decodes an ARP packet.
         @param packet Pointer to the packet data
     */
    void handleARP (const u_char* packet, PacketData pData);
    
    /**
        Decodes a TCP segment.
        @param ip Pointer to the IP datagram that it was encapsulated in
        @param packet Pointer to the packet data
        @param tcpCon Struct containing source/dest IPs
        @param ttl Time to live value
     */
    void handleTCP (const struct sniffIP *ip, const u_char* packet, TCPConnection tcpCon, PacketData pData);
    
    /**
         Decodes a UDP segment.
         @param ip Pointer to the IP datagram that it was encapsulated in
         @param packet Pointer to the packet data
         @param packetLen The length of the captured packet
         @param ttl Time to live value
     */
    void handleUDP (const struct sniffIP *ip, const u_char* packet, bpf_int32 packetLen, PacketData pData);
    
    /**
         Decodes an ICMP segment.
         @param ip Pointer to the IP datagram that it was encapsulated in
         @param packet Pointer to the packet data
         @param packetLen The length of the captured packet
         @param ttl Time to live value
     */
    void handleICMP (const struct sniffIP *ip, const u_char* packet, bpf_int32 packetLen, PacketData pData);
    
    /**
        Prints the payload of a TCP or UDP packet in hexadeciaml and ASCII (if printable)
        @param payload Pointer to the payload data
        @param len The length of the payload in bytes
     */
    void printPayload (const u_char* payload, int len);
    
    /**
        Prints a 16 byte line of the payload in hex and ASCII (if printable)
        @param payload The data to print
        @param len The length of data
        @param offset The byte offest in multiples of 16
     */
    void printHexAsciiLine (const u_char *payload, int len, int offset);
    
    /**
        Stores the source and destination IP addresses from an IP datagram
        @param source The source IP address
        @param dest The destination IP address
        @param isSourceSelf True if the source IP is one of this machine's IP addresses
        @param isDestSelf True if the destination IP is one of this machine's IP addresses
     */
    void storeIPAddresses (IPAddress source, IPAddress dest);
    
    /**
        Routes some packet data to the audio and graphics classes.
        @param protocol The protocol of the packet
        @param dataToSend Struct containing the packet data to be routed
     */
    void routeData (int protocol, PacketData dataToSend);
    
    /**
        Zeroes the protocol and PPS counters
     */
    void zeroCounters();
    
    Audio& audio;                          /// Reference to the audio class
    PacketGraphicsComponent& graphicsComp; /// Reference to the graphics component
    TextComponent& textComp;               /// Reference to the text component
    
    Listener* listener; /// Listener to this class
    
    Random random; /// Random
    
    ArraySorter sorter;             /// Array sorter object
    Array<IP> sourceIPs;            /// Array of source IPs and frequency
    Array<IP> destIPs;              /// Array of destination IPs and frequency
    Array<IPAddress> ipAddresses;   /// Array of this devices IP addresses
    Array<MACAddress> macAddresses; /// Array of this devices MAC addresses
    MACAddress broadcastMAC;        /// The broadcast MAC address
    IPAddress broadcastIP;          /// The broadcast IP address
    
    Array<int> udpPortNumbers;  /// Array of known UDP port numbers
    Array<String> udpPortNames; /// Array of corresponding known UDP port names
    Array<int> tcpPortNumbers;  /// Array of known TCP port numbers
    Array<String> tcpPortNames; /// Array of corresponding known TCP port names
    
    bool graphicTriggerLocks[4]; /// Locks protocols from triggering a graphics object for sampleInterval ms
    
    PacketData packetToVisualise[4]; /// Array of packetData structs to visualise for each protocol
    
    String outputString; /// String for printing packet data to the text component
    String ipListString; /// String for printing list of source and destination IPs the text component
    String statsString;  /// String for printing session stats to the text component
    bool shouldPrint;    /// True if text component tab is selected
    
    int sampleInterval; /// The interval in ms for visualising packet data
    
    LinkedList<TCPConnection> currentTCPConnections; /// Linked list of active TCP connections
    LinkedList<IPConnection> IPConnections;          /// Linked list of current IP connections
    
    int arpCount;      /// Number of ARP packets received this session
    int tcpCount;      /// Number of TCP packets received this session
    int udpCount;      /// Number of UDP packets received this session
    int icmpCount;     /// Number of ICMP packets received this session
    int arpLastCount;  /// Previous number of ARP packets received this session - used for working out packets per second
    int tcpLastCount;  /// Previous number of TCP packets received this session - used for working out packets per second
    int udpLastCount;  /// Previous number of UDP packets received this session - used for working out packets per second
    int icmpLastCount; /// Previous number of ICMP packets received this session - used for working out packets per second
    float arpPps;      /// ARP packets per second
    float tcpPps;      /// TCP packets per second
    float udpPps;      /// UDP packets per second
    float icmpPps;     /// ICMP packets per second
    
    CriticalSection sharedMem; /// Critical section for thread-safe shared vairable access
};
#endif /* PacketParser_hpp */
