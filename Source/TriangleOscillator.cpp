//
//  TriangleOscillator.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 17/01/2016.
//
//

#include "TriangleOscillator.hpp"

TriangleVoice::TriangleVoice (Audio& audio_) : BaseOscillator (audio_, 0)
{
    isVibratoOn = false;
}

TriangleVoice::~TriangleVoice()
{
    
}

bool TriangleVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<TriangleSound*> (sound) != nullptr;
};

void TriangleVoice::setSTKFreq (float frequency)
{
    setHarmonics (frequency);
    baseFreq = frequency;
}

void TriangleVoice::resetSTKGen()
{
    for (int i = 0; i < 10; i++)
        sineGen[i].reset();
    
    vibGen.reset();
}

float TriangleVoice::renderWaveshape()
{
    if (isVibratoOn)
    {
        float relativeDepth = vDepth * baseFreq;
        setHarmonics (baseFreq + (vibGen.tick() * relativeDepth));
    }
    
    float output = 0;
    for (int i = 0; i < 10; ++i)
    {
        output += sineGen[i].tick() * (1 / pow (harmonics[i], 2));
    }
    return output;
}

void TriangleVoice::vibratoOn (bool isOn)
{
    isVibratoOn = isOn;
}

void TriangleVoice::setVibRateDepth (float vibRate, float vibDepth)
{
    vibGen.setFrequency (vibRate);
    vDepth = vibDepth;
}

void TriangleVoice::setHarmonics (float frequency)
{
    for (int i = 0; i < 10; i++)
    {
        float harmFreq = frequency * harmonics[i];
        if (harmFreq < 22100)
        {
            sineGen[i].setFrequency(harmFreq);
        }
        else
        {
            sineGen[i].setFrequency (0);
        }
    }
}