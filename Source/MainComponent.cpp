/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainContentComponent::MainContentComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_, PacketGraphicsComponent& graphicsComp_, TextComponent& textComp_) : tabs (audio_, packetSniffer_, packetParser_, graphicsComp_, textComp_)
{
    setSize (600, 725);
    addAndMakeVisible (&tabs);
}

MainContentComponent::~MainContentComponent()
{

}

void MainContentComponent::paint (Graphics& g)
{

}

void MainContentComponent::resized()
{
    tabs.setBounds (getBounds());
}