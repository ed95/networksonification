//
//  LinkedList.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 21/03/2016.
//
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
     Template class to store and manipulate a linked list of objects.
 */
template <class T>
class LinkedList
{
public:
    /**
         Constructor. Creates an empty list.
     */
    LinkedList() : head (nullptr), tail (nullptr)
    {
    }
    
    /**
         Destructor. Clears up the contents of the list.
     */
    ~LinkedList()
    {
        deleteAll();
    }
    
    /**
         Returns the number of items in the list.
         @return The size of the list
     */
    int size() const
    {
        int total  = 0;
        
        for (Node* current = head; current != nullptr; current = current->next)
        {
            ++total;
        }
        
        return total;
    }
    
    /**
         Adds an item to the end of the list.
         @param newValue value to be added to the end of the list
     */
    void insertAtTail (T newData)
    {
        Node* newNode = new Node;
        
        newNode->data = newData;
        newNode->next = nullptr;
        
        if (tail == nullptr)
        {
            newNode->next = tail;
            tail = newNode;
            head = newNode;
            return;
        }
        
        tail->next = newNode;
        tail = tail->next;
    }
    
    /**
         Adds an item to the start of the list.
         @param newValue value to be added to the start of the list
     */
    void insertAtHead (T newData)
    {
        Node* newNode = new Node;
        
        newNode->data = newData;
        newNode->next = head;
        head = newNode;
        
        if (head->next == nullptr)
            tail = newNode;
    }
    
    /**
         Removes an item from the array.
         @param index the index of the element to remove
     */
    void remove (int index)
    {
        Node* current = head;
        
        if (current == nullptr)
            return;
        
        Node* previous = nullptr;
        while (current != nullptr)
        {
            if (index-- == 0)
            {
                if (previous == nullptr)
                    head = current->next;
                else
                    previous->next = current->next;
                
                delete current;
                break;
            }
            else
            {
                previous = current;
                current = current->next;
            }
        }
    }
    
    /**
         Returns the item at a given index in the list, or the end if index is out of range.
         @param index The index of the element to be returned
         @return The value of the item at index
     */
    T get (int index) const
    {
        Node* temp = head;
        
        while ((--index >= 0) && (temp->next != nullptr))
            temp = temp->next;
        
        return temp->data;
    }
    
    /**
        Sets the data of the node at the index to a new value
        @param index The index of the node to change
        @param newData The new data to add
     */
    void set (int index, T newData)
    {
        Node* temp = head;
        
        while ((--index >= 0) && (temp->next != nullptr))
            temp = temp->next;
        
        temp->data = newData;
    }
    
    /**
         Returns true if the list is empty
         @return True if empty
     */
    bool isEmpty() const
    {
        return head == nullptr;
    }
    
    /**
         Iterates the list, calling the delete operator on all of its elements leaving the list empty.
     */
    void deleteAll()
    {
        while (head != nullptr)
        {
            Node* temp = head;
            head = head->next;
            delete temp;
        }
    }
    
private:
    
    /**
        Struct containing template data and a pointer to the next node in the list
     */
    struct Node
    {
        T data;
        Node* next;
    };
    
    Node* head; /// start of the list
    Node* tail; /// end of the list
};

#endif /* LinkedList_hpp */
