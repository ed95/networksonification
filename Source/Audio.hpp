//
//  Audio.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#ifndef Audio_hpp
#define Audio_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioParameters.h"
#include "DataQueue.hpp"
#include "Network.h"
#include "LinkedList.hpp"

#define TCPTIMERID -1
#define TCPWINDOWTIME 1000

/**
    Struct containing information about a sustained TCP note.
 */
struct TCPNote
{
    int ID;
    int noteNum;
    int currentCount;
    float rate;
};

/**
    Class for all audio processing.
 */
class Audio : public AudioIODeviceCallback,
              public MultiTimer
{
public:
    /**
        Constructor.
     */
    Audio();
    
    /**
         Destructor.
     */
    ~Audio();
    
    /**
        Audio callback. Inherited from AudioIODeviceCallback.
     */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /**
        Inherited from AudioIODeviceCallback.
     */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /**
         Inherited from AudioIODeviceCallback.
     */
    void audioDeviceStopped() override;
    
    /**
         Inherited from MultiTimer.
     */
    void timerCallback (int timerID) override;
    
    /**
        Triggers a MIDI note based on network data.
        @param packet A packetData struct containing information about the packet
     */
    void triggerNoteWithData (PacketData packet);
    
    /**
        Stops all currently playing MIDI notes.
     */
    void stopAllNotes();
    
    /** 
         Returns the audio device manager.
         @return Reference to the AudioDeviceManager object
     */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}

    /** 
        Updates the audio parameters from the GUI using a lock-free queue 
     */
    void updateParamatersFromQueue();
    
    /**
        Maps an IP packet time to live value to a MIDI note velocity.
        @param ttl The TTL value of the packet
        @return The MIDI note velocity
     */
    int velocityMapTTL (int ttl);
    
    /**
         Maps a some packet data to a MIDI note.
         @param data The packet data to be mapped. TCP/UDP/ICMP - payload size, ARP - ARP operation.
         @param protocol The packet protocol
         @return The MIDI note number
     */
    int noteMap (int data, int protocol);
    
    /**
        Constructs a major or minor scale from a random root note.
        @return An array of the notes in the scale
     */
    Array<int> constructRandomScale();
    
    /**
        Checks if a given note number is currently a sustained TCP note.
        @param noteToGet The note number to test
        @return The TCPNote struct for the MIDI note provided, ID is -1 if not found
     */
    TCPNote getTCPNote (int noteToGet);
    
    /**
        Maps some network data to an audio parameter.
        @param data The network data to be mapped.
     */
    void mapDataToParameter (DataToBeMapped data);
    
    /**
        Linearly maps an input between a given range to an output between a given range.
        @param inMin The input range minimum
        @param inMax The input range maximum
        @param outMin The output range minimum
        @param outMax The output range maximum
        @param input The input to be linearly mapped
        @return The input linearly mapped to the output range
     */
    float linearMap (float inMin, float inMax, float outMin, float outMax, float input);

//====================================================================================
    
    //PARAMETERS
    FloatParameter* sampleRate;         /// The sample rate
    FloatParameter* masterLevel;        /// The master level
    FloatParameter* aAttack[4];         /// Amplitude envelope attack
    FloatParameter* aDecay;             /// Amplitude envelope decay
    FloatParameter* aSustain;           /// Amplitude envelope sustain
    FloatParameter* aRelease[4];        /// Amplitude envelope release
    FloatParameter* fAttack;            /// Filter envelope attack
    FloatParameter* fDecay;             /// Filter envelope decay
    FloatParameter* fRelease;           /// Filter envelope release
    FloatParameter* fSustain;           /// Filter envelope sustain
    FloatParameter* filterQ;            /// Filter Q
    FloatParameter* oscillatorLevel[4]; /// Level for the oscillators
    FloatParameter* reverbRoomSize;     /// Reverb room size
    FloatParameter* reverbWidth;        /// Reverb stereo width
    
    IntegerParameter* filterCutoff; /// Filter cutoff
    IntegerParameter* filterType;   /// Filter type
    IntegerParameter* ARPChannel;   /// ARP
    IntegerParameter* TCPChannel;   /// TCP
    IntegerParameter* UDPChannel;   /// UDP
    IntegerParameter* ICMPChannel;  /// ICMP
    
    Array<FloatParameter*> floatParams; /// Array of floating-point parameters
    Array<IntegerParameter*> intParams; /// Array of integer parameters
    int intOffset;                      /// Integer parameter offset
    
    DataQueue<ParameterPair> audioParametersQueue; /// Lock free queue for audio parameters
    DataQueue<DataToBeMapped> networkDataQueue;    /// Lock free queue for network data
    
private:
    AudioDeviceManager audioDeviceManager; /// Audio device manager
    Synthesiser synthesiser;               /// Synthesiser
    MidiMessageCollector mmCollector;      /// MIDI message collector
    
    Array<int> currentNotes[4];   /// Array of currently playing notes
    bool noteTriggerLocks[4];     /// Locks for triggering notes
    int noteLength[4];            /// Length of notes
    LinkedList<TCPNote> TCPNotes; /// Linked list of currently sustained TCP notes

    stk::FreeVerb reverb;            /// Reverb effect
    Reverb::Parameters reverbParams; /// Parameters for reverb effect
    
    Array<int> scaleToUse; /// Musical scale to be used - randomised at start of program
    
    Random random; /// JUCE random object
    
    int largestPayloadSize[4]; /// Stores the largest payload size received so far
    int payloadSampleCount[4]; /// Increment until it reaches payloadSampleSize
    int payloadSampleSize[4];  /// The number of samples to collect for each protocol
    
    int arpWeights[2][8]; /// Weightings for ARP/RARP note num selection
};
#endif /* Audio_hpp */
