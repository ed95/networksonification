//
//  PacketGraphicsComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 05/03/2016.
//
//

#include "PacketGraphicsComponent.hpp"

PacketGraphicsComponent::PacketGraphicsComponent() : shouldDrawGraphics (false), largestPayloadSize (0), payloadSampleSize (0)
{
    packetColours.add (Colours::maroon); // ARP
    packetColours.add (Colours::midnightblue); // TCP
    packetColours.add (Colours::darkgreen); // UDP
    packetColours.add (Colours::white); // ICMP
}

PacketGraphicsComponent::~PacketGraphicsComponent()
{
    
}

void PacketGraphicsComponent::resized()
{
}

void PacketGraphicsComponent::paint (Graphics &g)
{
    
}

void PacketGraphicsComponent::timerCallback()
{
    //update positions
    for (int i = 0; i < blobs.size(); i++)
    {
        if (!blobs.getUnchecked(i)->step())
        {
            blobs.remove (i);
        }
    }
}

void PacketGraphicsComponent::newPacket (PacketData data)
{
    if (blobs.size() < 50)
    {
        int size = data.payloadSize == -1 ? 50 : payloadSizeMap (data.payloadSize);
        
        Point<float> startPos (Random::getSystemRandom().nextFloat() * getBounds().getWidth(), getBounds().getBottom());
        
        PacketBlob* newBlob = new PacketBlob (startPos, size, data, packetColours);
        addAndMakeVisible (newBlob);
        blobs.add (newBlob);
    }
}

void PacketGraphicsComponent::setActive (bool state)
{
    shouldDrawGraphics = state;
    
    if (state)
    {
        startTimerHz (60);
    }
    else
    {
        if (isTimerRunning())
            stopTimer();
        
        blobs.clear();
    }
}

bool PacketGraphicsComponent::isActive()
{
    return shouldDrawGraphics;
}

int PacketGraphicsComponent::payloadSizeMap(int payloadSize)
{
    int circleSize;
    
    if (payloadSize > 65535)
        payloadSize = 65535;
    
    if (payloadSize >= largestPayloadSize)
        largestPayloadSize = payloadSize;
    
    if (payloadSampleSize < 20)
    {
        circleSize = rand() % 150;
        payloadSampleSize++;
    }
    else
    {
        circleSize = ((double)payloadSize / largestPayloadSize) * 150.f;
    }
    
    return circleSize < 5 ? 5 : circleSize;
}

void PacketGraphicsComponent::reset()
{
    largestPayloadSize = 0;
    payloadSampleSize = 0;
}

Colour PacketGraphicsComponent::getColourAtIndex (int index)
{
    if (index >= ARPPACKET && index <= ICMPPACKET)
        return packetColours.getUnchecked (index);
    
    return Colours::white;
}

void PacketGraphicsComponent::setColourAtIndex (int index, Colour newColour)
{
    if (index >= ARPPACKET && index <= ICMPPACKET)
    {
        packetColours.set (index, newColour);
        if (listener != nullptr)
            listener->colourChanged (index, newColour);
    }
}

void PacketGraphicsComponent::setListener (Listener* newListener)
{
    listener = newListener;
}