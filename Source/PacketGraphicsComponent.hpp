//
//  PacketGraphicsComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 05/03/2016.
//
//

#ifndef PacketGraphicsComponent_hpp
#define PacketGraphicsComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Network.h"

/**
    Class for visualising packets as blobs
 */
class PacketBlob  : public Component
{
public:
    /**
        Constructor.
        @param pos The starting position for this blob
        @param size The size of the blob
        @param pData A packetData struct containing info about the packet
        @param coloursArray The array of colours currently chosen for each protocol
     */
    PacketBlob (const Point<float>& pos, int size, PacketData pData, Array<Colour> coloursArray) :
                position (pos),
                speed (Random::getSystemRandom().nextFloat() *  4.0f - 2.0f,
                       pData.ttl == -1 ? Random::getSystemRandom().nextFloat() * 10.0f - 12.0f :
                       ttlMap (pData.ttl) * 10.0f - 12.0f)
    {
        if (size < 4)
            size = 4;
        
        setSize (size, size);
        
        shouldDrawText = true;
        
        if (pData.packetType == ICMPPACKET ? size < 40 : size < 30)
            shouldDrawText = false;
        
        switch (pData.packetType) {
            case ARPPACKET:
                colour = coloursArray.getUnchecked (ARPPACKET);
                protocolLabel = "ARP";
                break;
            case TCPPACKET:
                colour = coloursArray.getUnchecked (TCPPACKET);
                protocolLabel = "TCP";
                break;
            case UDPPACKET:
                colour = coloursArray.getUnchecked (UDPPACKET);
                protocolLabel = "UDP";
                break;
            case ICMPPACKET:
                colour = coloursArray.getUnchecked (ICMPPACKET);
                protocolLabel = "ICMP";
                break;
                
            default:
                break;
        }
        
        step();
    }
    
    /**
        Moves the blob
        @return True if in bounds, false if out of bounds and should be deleted
     */
    bool step()
    {
        position += speed;
        
        setCentrePosition ((int) position.x,
                           (int) position.y);
        
        if (Component* parent = getParentComponent())
            return isPositiveAndBelow (position.x, (float) parent->getWidth())
            && position.y < (float) parent->getHeight();
        
        return false;
    }
    
    /**
        Inherited from Component.
     */
    void paint (Graphics& g)
    {
        g.setColour (colour);
        g.fillEllipse (2.0f, 2.0f, getWidth() - 4.0f, getHeight() - 4.0f);
        
        g.setColour (Colours::black);
        g.drawEllipse (2.0f, 2.0f, getWidth() - 4.0f, getHeight() - 4.0f, 1.0f);
        
        if (shouldDrawText)
        {
            g.setColour (colour.contrasting (1.f));
            g.drawText(protocolLabel, 2.f, 2.f, getWidth() - 4.f, getHeight() - 4.f, Justification::centred);

        }
    }
    
    /**
        Maps the packet's time to live value to a speed.
     */
    float ttlMap (int ttl)
    {
        if (ttl > 64)
        {
            return 0.f;
        }
        else
        {
            return 1.f - (ttl / 64.f);
        }
    }
    
private:
    Point<float> position; /// Position co-ordinate of the blob
    Point<float> speed;    /// Speed co-ordinate of the blob
    Colour colour;         /// Colour of the blob
    String protocolLabel;  /// Packet protocol
    bool shouldDrawText;   /// True if blob is big enough to fit text on, false if not
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PacketBlob)
};

/**
    Class for the visualising packets
 */
class PacketGraphicsComponent : public Component,
                                public Timer
{
public:
    /**
        Constructor.
     */
    PacketGraphicsComponent();
    
    /**
         Destructor.
     */
    ~PacketGraphicsComponent();
    
    /**
        Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
         Inherited from Timer.
     */
    void timerCallback() override;
    
    /**
        Sets whether graphics should be drawn.
        @param state True if this component is selected and graphics should be drawn, false if not
     */
    void setActive (bool state);
    
    /**
        Returns whether this component is active or not
        @return True if active, false if not
     */
    bool isActive();
    
    /**
        Called when a new packet should be visualised.
        @param packetData A struct containing the data to be visualised
     */
    void newPacket (PacketData data);
    
    /**
        Maps the payload size of a TCP or UDP segment to a blob size
        @param payloadSize The payload size in bytes of the segment
        @return The size of the corresponding blob
     */
    int payloadSizeMap (int payloadSize);
    
    /**
        Called when the graphics component is deselected.
     */
    void reset();
    
    /**
        Returns the colour at a given index of the colours array.
         @param index The index of the colour to get
         @return The colour at the index
     */
    Colour getColourAtIndex (int index);
    
    /**
        Sets a colour at a given index of the colours array
        @param index The index to change
        @param newColour The new colour
     */
    void setColourAtIndex (int index, Colour newColour);
    
    /**
     Listener class.
     */
    class Listener
    {
    public:
        /**
         Destructor.
         */
        virtual ~Listener(){}
        
        /**
            Called when the colour for a protocol changes.
            @param protocol The protocol this new colour applies to
            @param newColour The new colour
         */
        virtual void colourChanged (int protocol, Colour newColour) = 0;
    };
    
    
    /**
        Sets the listener.
        @param newListener The new listener
     */
    void setListener (Listener* newListener);
    
private:
    bool shouldDrawGraphics;      /// True if this component is selected and should draw graphics, false if not
    OwnedArray<PacketBlob> blobs; /// Array of the currently active packet blobs
    
    int largestPayloadSize; /// The largest payload size so far - used to determine size of blobs
    int payloadSampleSize;  /// Blobs will have random sizes until this sample size is met
    
    Array<Colour> packetColours; /// Array of colours for each packet protocol
    
    Listener* listener; /// The listener to this class
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PacketGraphicsComponent)
};

#endif /* PacketGraphicsComponent_hpp */
