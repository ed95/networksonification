/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainApplicationComponent.hpp"
#include "SettingsComponent.hpp"
#include "PacketGraphicsComponent.hpp"
#include "TextComponent.hpp"

/**
    Tab component containing all the application components.
 */
class TabbedApplication  : public TabbedComponent
{
public:
    /**
        Constructor.
        @param audio_ Reference to the Audio class @see Audio
        @param packetSniffer_ Reference to the PacketSniffer class @see PacketSniffer
        @param packetParser_ Reference to the PacketParser class @see PacketParser
        @param graphicsComp_ Reference to the PacketGraphicsComponent class @see PacketGraphicsComponent
        @param textComp_ Reference to the TextComponent class @see TextComponent
     */
    TabbedApplication (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_,
                       PacketGraphicsComponent& graphicsComp_, TextComponent& textComp_)
                      : TabbedComponent (TabbedButtonBar::TabsAtLeft), gComp (graphicsComp_), tComp (textComp_)
    {
        settingsComp = new SettingsComponent (audio_, packetSniffer_, graphicsComp_);
        mainAppComp = new MainApplicationComponent (audio_, packetSniffer_, packetParser_, graphicsComp_);
        
        addTab ("App", Colours::lightgrey, mainAppComp, true);
        addTab ("Settings", Colours::grey, settingsComp, true);
        addTab ("Graphics", Colours::lightgrey, &graphicsComp_, false);
        addTab ("Text Output", Colours::white, &textComp_, false);
    }
    
    /**
        Called when the tab changes.
     */
    void currentTabChanged (int newCurrentTabIndex, const String& newCurrentTabName)
    {
        if (newCurrentTabName == "App")
        {
            gComp.setActive (false);
            tComp.setActive (false);
            mainAppComp->setActive (true);
        }
        else if (newCurrentTabName == "Settings")
        {
            settingsComp->rememberCurrentNetworkSettings();
            gComp.setActive (false);
            tComp.setActive (false);
            mainAppComp->setActive (false);
        }
        else if (newCurrentTabName == "Graphics")
        {
            gComp.setActive (true);
            tComp.setActive (false);
            mainAppComp->setActive (false);
        }
        else if (newCurrentTabName == "Text Output")
        {
            gComp.setActive (false);
            tComp.setActive (true);
            mainAppComp->setActive (false);
        }
        
        for (int i = 0; i < getNumTabs(); i++)
        {
            if (i != newCurrentTabIndex)
            {
                Component *comp = getTabContentComponent (i);
                comp->postCommandMessage (0);
            }
        }
    }
private:
    ScopedPointer<SettingsComponent> settingsComp;       /// The settings component
    PacketGraphicsComponent& gComp;                      /// The graphics component
    TextComponent& tComp;                                /// The text component
    ScopedPointer<MainApplicationComponent> mainAppComp; /// The main application component
};

//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainContentComponent   : public Component
{
public:
    //==============================================================================
    MainContentComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_,
                          PacketGraphicsComponent& graphicsComp_, TextComponent& textComp_);
    
    ~MainContentComponent();

    void paint (Graphics&) override;
    void resized() override;
    
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
    TabbedApplication tabs;
};


#endif  // MAINCOMPONENT_H_INCLUDED
