//
//  SettingsComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/03/2016.
//
//

#include "SettingsComponent.hpp"

SettingsComponent::SettingsComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketGraphicsComponent& graphicsComp_) : headerLabel ("", "Settings"), audioLabel ("", "Audio Settings"), networkLabel ("", "Network Settings"), graphicsLabel ("", "Graphics Settings"), itemHeight (24), audioSettingsComp (audio_.getAudioDeviceManager(), 0, 2, 2, 2, false, false, true, false), networkSettingsComp (audio_, packetSniffer_), graphicsSettingsComp (packetSniffer_.getParser(), graphicsComp_)
{
    addAndMakeVisible (&headerLabel);
    addAndMakeVisible (&audioLabel);
    addAndMakeVisible (&networkLabel);
    addAndMakeVisible (&graphicsLabel);
    
    headerLabel.setFont (Font::bold);
    headerLabel.setFont (Font (25.f));
    headerLabel.setJustificationType (Justification::centred);
    
    audioLabel.setJustificationType (Justification::centred);
    networkLabel.setJustificationType (Justification::centred);
    graphicsLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (&audioSettingsComp);
    addAndMakeVisible (&networkSettingsComp);
    addAndMakeVisible (&graphicsSettingsComp);
}

SettingsComponent::~SettingsComponent()
{
    
}

void SettingsComponent::resized()
{
    Rectangle<int> r (proportionOfWidth (0.1f), 15, proportionOfWidth (0.8f), 3000);
    const int space = itemHeight / 4;
    
    headerLabel.setBounds (r.removeFromTop (itemHeight));
    r.removeFromTop (space * 3);
    
    audioLabel.setBounds (r.removeFromTop (itemHeight));
    r.removeFromTop (space);
    
    audioSettingsComp.setBounds (r.removeFromTop(itemHeight * 7));
    r.removeFromTop (space * 1.5);
    
    networkLabel.setBounds (r.removeFromTop(itemHeight));
    r.removeFromTop (space);
    
    networkSettingsComp.setBounds (r.removeFromTop(itemHeight * 7));
    r.removeFromTop (space * 1.5);
    
    graphicsLabel.setBounds (r.removeFromTop (itemHeight));
    r.removeFromTop (space);
    
    graphicsSettingsComp.setBounds (r.removeFromTop(itemHeight * 10));
}

void SettingsComponent::paint (juce::Graphics &g)
{
}

void SettingsComponent::rememberCurrentNetworkSettings()
{
    networkSettingsComp.rememberCurrentSettings();
}