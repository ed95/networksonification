//
//  GraphicsSettingsComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 16/03/2016.
//
//

#include "GraphicsSettingsComponent.hpp"

GraphicsSettingsComponent::GraphicsSettingsComponent(PacketParser& parser_, PacketGraphicsComponent& graphicsComp_) : parser (parser_), graphicsComp (graphicsComp_), itemHeight (24), selectedButton (-1)
{
    addAndMakeVisible (&sampleIntervalSlider);
    sampleIntervalSlider.addListener (this);
    sampleIntervalSlider.setRange (10, 2000, 1);
    sampleIntervalSlider.setSkewFactor (0.5);
    sampleIntervalSlider.setTextValueSuffix ("ms");
    sampleIntervalSlider.setValue (500);
    
    sampleIntervalLabel.setText ("Packet Sample Interval: ", dontSendNotification);
    sampleIntervalLabel.setJustificationType (Justification::centred);
    sampleIntervalLabel.attachToComponent (&sampleIntervalSlider, true);
    
    addAndMakeVisible (&infoTooltipImageComp);
    
    File location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory()
    .getChildFile ("Resources").getChildFile ("resources");
    
    if (!location.exists())
    {
        location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile ("resources");
    }
    
    File infoImagePath = File(location.getChildFile ("images/info.png"));
    infoTooltipImage = ImageFileFormat::loadFrom (infoImagePath);
    infoTooltipImageComp.setImage (infoTooltipImage);
    infoTooltipImageComp.setTooltip ("The sample interval for packet data to be visualised");
    
    for (int i = 0; i < 4; ++i)
    {
        addAndMakeVisible (&protocolColourButton[i]);
        protocolColourButton[i].addListener (this);
        protocolColourButton[i].setProtocolType (i);
        protocolColourButton[i].setTooltip ("Click to change colour, double click to reset to default");
        defaultColours[i] = graphicsComp.getColourAtIndex (i);
    }
    
    parser.setSampleInterval (sampleIntervalSlider.getValue());
}

GraphicsSettingsComponent::~GraphicsSettingsComponent()
{
    
}

void GraphicsSettingsComponent::resized()
{
    Rectangle<int> r (proportionOfWidth (0.35f), 15, proportionOfWidth (0.6f), 3000);
    const int space = itemHeight / 5;
    
    sampleIntervalSlider.setBounds (r.removeFromTop (itemHeight).withWidth (proportionOfWidth (0.55)));
    r.removeFromTop (space * 3);
    
    infoTooltipImageComp.setBounds (sampleIntervalSlider.getRight() + 5, sampleIntervalSlider.getY(),
                                    sampleIntervalSlider.getHeight(), sampleIntervalSlider.getHeight());
    
    r.removeFromTop (space * 3);
    
    protocolColourButton[0].setBounds (r.removeFromTop (itemHeight * 3).withWidth (itemHeight * 3).withX (50));
    
    for (int i = 1; i < 4; ++i)
    {
        protocolColourButton[i].setBounds (protocolColourButton[i - 1]
                                           .getBounds().withX (protocolColourButton[i - 1]
                                                               .getRight() + 20));
    }
}

void GraphicsSettingsComponent::paint (Graphics &g)
{
    
}

void GraphicsSettingsComponent::timerCallback(int timerID)
{
    if (timerID == -1)
    {
        parser.setSampleInterval (sampleIntervalSlider.getValue());
    }
    else
    {
        buttonClickedFlag[timerID] = false;
    }
}

void GraphicsSettingsComponent::sliderValueChanged (Slider *slider)
{
    if (slider == &sampleIntervalSlider)
    {
        startTimer (-1, 1500);
    }
}

void GraphicsSettingsComponent::buttonClicked (Button *button)
{
    for (int i = 0; i < 4; ++i)
    {
        if (button == &protocolColourButton[i])
        {
            if (buttonClickedFlag[i])
            {
                protocolColourButton[i].newColour (defaultColours[i]);
                graphicsComp.setColourAtIndex (i, defaultColours[i]);
                return;
            }
            
            ColourSelector* colourSelector = new ColourSelector();
            colourSelector->setName ("Protocol Colour");
            colourSelector->setCurrentColour (graphicsComp.getColourAtIndex (i));
            colourSelector->addChangeListener (this);
            colourSelector->setColour (ColourSelector::backgroundColourId, Colours::transparentBlack);
            colourSelector->setSize (300, 400);
            
            selectedButton = i;
            
            buttonClickedFlag[i] = true;
            startTimer (i, 500);
            
            CallOutBox::launchAsynchronously (colourSelector, getScreenBounds(), nullptr);
        }
    }
}

void GraphicsSettingsComponent::changeListenerCallback (ChangeBroadcaster *source)
{
    if (ColourSelector* cs = dynamic_cast<ColourSelector*> (source))
    {
        graphicsComp.setColourAtIndex (selectedButton, cs->getCurrentColour());
        protocolColourButton[selectedButton].newColour (cs->getCurrentColour());
    }
}