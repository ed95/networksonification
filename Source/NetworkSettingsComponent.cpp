//
//  NetworkSettingsComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 12/02/2016.
//
//

#include "NetworkSettingsComponent.hpp"

NetworkSettingsComponent::NetworkSettingsComponent (Audio& audio_, PacketSniffer& sniffer_) : settingsChanged (false), audio (audio_), sniffer (sniffer_), itemHeight (24)
{
    //promisc
    promiscModeComboBox.addItem ("Off", 1);
    promiscModeComboBox.addItem ("On", 2);
    promiscModeComboBox.setText ("Off");
    addAndMakeVisible (&promiscModeComboBox);
    promiscModeComboBox.addListener (this);
    
    promiscModeLabel.setText ("Promiscuous Mode: ", dontSendNotification);
    promiscModeLabel.setJustificationType (Justification::centredRight);
    promiscModeLabel.attachToComponent (&promiscModeComboBox, true);
    
    //devices
    const Array<String> devices = sniffer.getAvailableDeviceTypes();
    
    if (devices.size() > 1)
    {
        for (int i = 0; i < devices.size(); ++i)
            deviceComboBox.addItem (devices.getUnchecked(i), i + 1);
        
        if (devices.contains ("en1"))
            deviceComboBox.setText ("en1");
        else
            deviceComboBox.setSelectedId (1);
        
        addAndMakeVisible (deviceComboBox);
        deviceComboBox.addListener (this);
        
        deviceLabel.setText ("Capture Devices: ", dontSendNotification);
        deviceLabel.setJustificationType (Justification::centredRight);
        deviceLabel.attachToComponent (&deviceComboBox, true);
    }
    
    //filter
    addAndMakeVisible (&filterTextEditor);
    filterTextEditor.addListener (this);
    filterTextEditor.setTextToShowWhenEmpty ("Enter a filter expression here...", Colours::lightblue);
    
    filterLabel.setText ("Filter Expression: ", dontSendNotification);
    filterLabel.setJustificationType (Justification::centredRight);
    filterLabel.attachToComponent (&filterTextEditor, true);
    
    //apply button
    addAndMakeVisible (&applyButton);
    applyButton.addListener (this);
    applyButton.setButtonText ("Apply");
    applyButton.setEnabled (false);
    
    promiscComboBoxPrev = promiscModeComboBox.getSelectedId();
    deviceComboBoxPrev = deviceComboBox.getSelectedId();
    filterTextPrev = filterTextEditor.getText();
    
    initialiseSniffer();
}

NetworkSettingsComponent::~NetworkSettingsComponent()
{

}

void NetworkSettingsComponent::resized()
{
    Rectangle<int> r (proportionOfWidth (0.35f), 15, proportionOfWidth (0.6f), 3000);
    const int space = itemHeight / 5;

    promiscModeComboBox.setBounds (r.removeFromTop (itemHeight));
    r.removeFromTop (space * 3);
    
    deviceComboBox.setBounds (r.removeFromTop (itemHeight));
    r.removeFromTop (space * 3);
    
    filterTextEditor.setBounds (r.removeFromTop (itemHeight * 1.5));
    r.removeFromTop (space * 3);
    
    applyButton.setBounds (r.removeFromTop (itemHeight));
}

void NetworkSettingsComponent::paint (Graphics& g)
{
    
}

void NetworkSettingsComponent::comboBoxChanged (ComboBox *comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &deviceComboBox)
    {
        bool haveSettingsChanged = compareSettings();
        settingsChanged = haveSettingsChanged;
        applyButton.setEnabled (haveSettingsChanged);
    }
    else if(comboBoxThatHasChanged == &promiscModeComboBox)
    {
        if (promiscModeComboBox.getSelectedId() == 2)
            AlertWindow::showMessageBox(AlertWindow::AlertIconType::InfoIcon, "Warning", "Make sure you have permission from the network administrator to run this program in promiscuous mode.");
        
        bool haveSettingsChanged = compareSettings();
        settingsChanged = haveSettingsChanged;
        applyButton.setEnabled (haveSettingsChanged);
    }
}

void NetworkSettingsComponent::buttonClicked (juce::Button *button)
{
    if (button == &applyButton)
    {
        if (settingsChanged)
        {
            //stop all active notes
            ParameterPair param;
            param.t = ParameterPair::Flag;
            param.identifier = Parameters::pStopAllNotes;
            audio.audioParametersQueue.addToFifo (&param, 1);
            
            settingsChanged = false;
            applyButton.setEnabled (false);
            rememberCurrentSettings();
            
            initialiseSniffer();
        }
    }
}


void NetworkSettingsComponent::textEditorReturnKeyPressed (TextEditor& editor)
{
    if (&editor == &filterTextEditor)
    {
        bool haveSettingsChanged = compareSettings();
        settingsChanged = haveSettingsChanged;
        applyButton.setEnabled (haveSettingsChanged);
    } 
}

bool NetworkSettingsComponent::initialiseSniffer()
{    
    zerostruct (info);
    int result = sniffer.initialise (deviceComboBox.getText().toUTF8(), filterTextEditor.getText().toUTF8(), promiscModeComboBox.getSelectedId() - 1, &info);
    if (result != 0)
    {
        sniffer.setSnifferState (false);
        switch (result) {
            case ERR_NODEVICES:
                AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "No network devices found");
                return false;
            case ERR_NETMASK:
                AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "Couldn't get netmask for device " + String (info.currentDevice));
                return false;
            case ERR_DEVICEOPEN:
                AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "Couldn't open device " + String (info.currentDevice));
                return false;
            case ERR_NOTETHERNET:
               AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", String (info.currentDevice) + " is not an Ethernet");
                return false;
            case ERR_FILTERPARSE:
                AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "Couldn't parse filter " + String (info.filterExpression));
                return false;
            case ERR_FILTERINSTALL:
                AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "Couldn't install filter " + String (info.filterExpression));
                return false;
            default:
                return true;
        }
    }
    
    return true;
}

void NetworkSettingsComponent::rememberCurrentSettings()
{
    deviceComboBoxPrev = deviceComboBox.getSelectedId();
    promiscComboBoxPrev = promiscModeComboBox.getSelectedId();
    filterTextPrev = filterTextEditor.getText();
}

bool NetworkSettingsComponent::compareSettings()
{
    if (deviceComboBox.getSelectedId() != deviceComboBoxPrev || promiscModeComboBox.getSelectedId() != promiscComboBoxPrev || filterTextEditor.getText() != filterTextPrev)
        return true;
    
    return false;
}