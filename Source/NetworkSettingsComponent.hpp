//
//  NetworkSettingsComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 12/02/2016.
//
//

#ifndef NetworkSettingsComponent_hpp
#define NetworkSettingsComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PacketSniffer.hpp"

/**
    Class for the network settings component.
 */
class NetworkSettingsComponent : public Component,
                                 public ComboBox::Listener,
                                 public TextEditor::Listener,
                                 public Button::Listener
{
public:
    /**
        Constructor.
        @param audio_ Reference to the Audio class @see Audio
        @param snifer_ Reference to the PacketSniffer class @see PacketSniffer
     */
    NetworkSettingsComponent (Audio& audio_, PacketSniffer& sniffer_);
    
    /**
        Destructor.
     */
    ~NetworkSettingsComponent();
    
    /**
        Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
         Inherited from ComboBox::Listener.
     */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    /**
         Inherited from Button::Listener.
     */
    void buttonClicked (Button* button) override;
    
    /**
         Inherited from TextEditor::Listener.
     */
    void textEditorReturnKeyPressed (TextEditor& editor) override;
    
    /**
        Re-initialises the packet when the apply button is pressed
         @return True if fine, false if error.
     */
    bool initialiseSniffer();
    
    /**
        Called when settings page is opened to store current settings
     */
    void rememberCurrentSettings();
    
    /**
        Compares the current settings to the stored ones.
        @return True if settings have changed, false if not
     */
    bool compareSettings();
    
    bool settingsChanged; /// True if settings have changed, false if not.
    
private:
    Audio& audio;           /// Reference to the Audio class
    PacketSniffer& sniffer; /// Reference to the PacketSniffer class
    
    ComboBox promiscModeComboBox; /// ComboBox to set promiscuous mode on or off
    Label promiscModeLabel;       /// Label for promiscuous mode ComboBox
    ComboBox deviceComboBox;      /// ComboBox containing available capture devices
    Label deviceLabel;            /// Label for device ComboBox
    TextEditor filterTextEditor;  /// Filter text input
    Label filterLabel;            /// Label for filter text
    
    TextButton applyButton; /// Button for applying settings
    
    int itemHeight; /// Height of the items
    
    int promiscComboBoxPrev; /// Stores the previous promiscuous mode setting
    int deviceComboBoxPrev;  /// Stores the previous capture device setting
    String filterTextPrev;   /// Stores the previous filter text
    
    NetworkInfo info; /// Network info struct
};
#endif /* NetworkSettingsComponent_hpp */
