/*
  ==============================================================================

    AudioControls.cpp
    Created: 18 Dec 2015 2:29:53pm
    Author:  Edward Davies

  ==============================================================================
*/

#include "AudioControls.h"

AudioControls::AudioControls (Audio& audio_) : audio (audio_)
{
    addAndMakeVisible (&masterLevelSlider);
    masterLevelSlider.setRange (0, 1.f, 0.01);
    masterLevelSlider.addListener (this);
    masterLevelSlider.setValue (1.f);
    masterLevelSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
    
    File location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory()
                                .getChildFile ("Resources").getChildFile ("resources");
    
    if (!location.exists())
    {
        location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile ("resources");
    }
    
    File volImagePath = File(location.getChildFile ("images/vol.png"));
    volImage = ImageFileFormat::loadFrom (volImagePath);
    volImageComponent.setImage (volImage);
    addAndMakeVisible (&volImageComponent);
}

AudioControls::~AudioControls()
{
    
}

void AudioControls::resized()
{
    volImageComponent.setBounds(20, (getHeight() / 2) - proportionOfWidth (0.05), proportionOfWidth(0.1), proportionOfWidth(0.1));
    masterLevelSlider.setBounds (volImageComponent.getRight(), volImageComponent.getY(), proportionOfWidth (0.8), volImageComponent.getHeight());
}

void AudioControls::paint (Graphics &g)
{
    
}

void AudioControls::sliderValueChanged (Slider *slider)
{
    if (slider == &masterLevelSlider)
    {
        ParameterPair param;
        param.t = ParameterPair::Float;
        param.identifier = Parameters::pMasterLevel;
        param.fVal = masterLevelSlider.getValue();
        audio.audioParametersQueue.addToFifo (&param, 1);
    }
}