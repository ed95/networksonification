//
//  DataQueue.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#ifndef DataQueue_hpp
#define DataQueue_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
     Lock-free ring buffer FIFO template class
 */
template <class T>
class DataQueue
{
public:
    /**
        Constructor. Initialises the abstract FIFO with 1024 elements.
     */
    DataQueue()  : abstractFifo (1024)
    {
    }
    
    /**
        Add some data to the FIFO.
        @param someData A pointer to the data to add to the queue
        @param numItems The number of items to add to the queue
     */
    void addToFifo (const T* someData, int numItems)
    {
        int start1, size1, start2, size2;
        abstractFifo.prepareToWrite (numItems, start1, size1, start2, size2);
        
        if (size1 > 0)
            memcpy ((void*)(buffer + start1), someData, size1 * sizeof (T));
        
        if (size2 > 0)
            memcpy ((void*)(buffer + start2), someData + size1, size2 * sizeof (T));
        
        abstractFifo.finishedWrite (size1 + size2);
    }
    
    /**
        Read some data from the FIFO into a variable.
         @param someData A pointer to the object you want to fill
         @param numItems The number of items to remove from the queue
     */
    void readFromFifo (T* someData, int numItems)
    {
        int start1, size1, start2, size2;
        abstractFifo.prepareToRead (numItems, start1, size1, start2, size2);
        
        if (size1 > 0)
            memcpy (someData, (void*)(buffer + start1), size1 * sizeof (T));
        
        if (size2 > 0)
            memcpy (someData + size1, (void*)(buffer + start2), size2 * sizeof (T));
        
        abstractFifo.finishedRead (size1 + size2);
    }
    
    /**
         Returns the number of elements ready to get from the queue.
         @return The number of elements ready to get from the queue
     */
    int getNumReady() const
    {
        return abstractFifo.getNumReady();
    }
    
private:
    AbstractFifo abstractFifo; /// The FIFO object
    T buffer [1024];           /// The buffer
};

#endif /* DataQueue_hpp */
