//
//  TriangleOscillator.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 17/01/2016.
//
//

#ifndef TriangleOscillator_hpp
#define TriangleOscillator_hpp

#include <stdio.h>
#include "BaseOscillator.hpp"

/**
    Triangle wave sound.
 */
class TriangleSound : public SynthesiserSound
{
public:
    /**
         Constructor.
     */
    TriangleSound() {};
    
    /**
         Destructor.
     */
    ~TriangleSound() {};
    
    /**
         Inherited from SynthesiserSound. Applies to all notes.
     */
    bool appliesToNote (int midiNoteNumber) override { return true; };
    
    /**
         Inherited from SynthesiserSound. Applies to MIDI channel 1.
     */
    bool appliesToChannel (int midiChannel) override
    {
        if (midiChannel == 0)
            return true;
        return false;
    };
    
private:
    int protocol;
};

/**
     Triangle wave voice.
 */

class TriangleVoice : public BaseOscillator
{
public:
    
    /**
         Constructor.
         @param Reference to the Audio class. @see Audio
     */
    TriangleVoice (Audio& audio_);
    
    /**
         Destructor.
     */
    ~TriangleVoice();
    
    /**
         Returns true if the voice can play a sound.
     */
    bool canPlaySound (SynthesiserSound* sound) override;
    
    /**
         Sets the frequency of the STK generator. Inherited from BaseOscillator.
         @param frequency The frequency to set the generator
     */
    void setSTKFreq (float frequency) override;
    
    /**
         Resets the STK generator. Inherited from BaseOscillator.
     */
    void resetSTKGen() override;
    
    /**
         Renders the waveshape. Inherited from BaseOscillator.
         @return The sample value for this waveshape
     */
    float renderWaveshape() override;

    /**
         Sets whether vibrato should be on or off for this voice. Inherited from BaseOscillator.
         @param isOn True if vibrato should be on, false if off
     */
    void vibratoOn (bool isOn) override;
    
    /**
         Sets the rate and depth of the vibrato effect. Inherited from BaseOscillator.
         @param vibRate The rate in Hz of the vibrato LFO
         @param vibDepth The relative depth of the effect between 0 and 1
     */
    void setVibRateDepth (float vibRate, float vibDepth) override;
    
    void setHarmonics (float frequency);
    
private:
    int numHarmonics;
    stk::SineWave sineGen[10];                               /// Array of 10 STK sine wave generators
    int harmonics[10] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19}; /// Odd harmonics to generate
    
    stk::SineWave vibGen;                                    /// Vibrato LFO
    bool isVibratoOn;                                        /// True if vibrato effect should be applied, false if not
    float baseFreq;                                          /// Stores the base frequency of the note
    float vDepth;                                            /// Relative depth of the vibrato effect
    float vRate;                                             /// Rate in Hz of the vibrato LFO
};

//==============================================================================
#endif /* TriangleOscillator_hpp */
