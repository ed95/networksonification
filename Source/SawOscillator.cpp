//
//  SawOscillator.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 16/12/2015.
//
//

#include "SawOscillator.hpp"

SawVoice::SawVoice (Audio& audio_) : BaseOscillator (audio_, 2)
{
    isVibratoOn = false;
}

SawVoice::~SawVoice()
{
    
}

bool SawVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<SawSound*> (sound) != nullptr;
};

void SawVoice::setSTKFreq (float frequency)
{
    sawGen.setFrequency (frequency);
    baseFreq = frequency;
}

void SawVoice::resetSTKGen()
{
    sawGen.reset();
    vibGen.reset();
}

float SawVoice::renderWaveshape()
{
    if (isVibratoOn)
    {
        float relativeDepth = vDepth * baseFreq;
        sawGen.setFrequency (baseFreq + (vibGen.tick() * relativeDepth));
        return sawGen.tick();
    }
    else
    {
        return sawGen.tick();
    }
}

void SawVoice::vibratoOn (bool isOn)
{
    isVibratoOn = isOn;
}

void SawVoice::setVibRateDepth (float vibRate, float vibDepth)
{
    vibGen.setFrequency (vibRate);
    vDepth = vibDepth;
}