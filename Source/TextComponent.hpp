//
//  TextComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 07/03/2016.
//
//

#ifndef TextComponent_hpp
#define TextComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
    Prints the decoded packet data
 */
class TextComponent : public Component,
                      public Timer
{
public:
    /**
        Constructor.
     */
    TextComponent();
    
    /**
         Destructor.
     */
    ~TextComponent();
    
    /**
        Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
        Adds a new string to the print queue
         @param str The string to add
     */
    void addToQueue (String str);
    
    /**
        Called when the component is selected or deselected. Sets whether text should be outputted.
        @param state True if selected, false if not
     */
    void setActive (bool state);
    
    /**
        Returns the state of the component.
         @return True if active, false if not
     */
    bool isActive();
    
    /**
         Clears the print queue.
     */
    void reset();
    
    /**
        Inherited from Timer.
     */
    void timerCallback() override;
    
private:
    TextEditor textOut;       /// The TextEditor component
    bool shouldPrintText;     /// True if text should be printed, false if not
    Array<String> printQueue; /// Array of string to be printed
};

#endif /* TextComponent_hpp */
