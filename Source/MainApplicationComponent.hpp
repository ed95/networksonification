//
//  MainApplicationComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/03/2016.
//
//

#ifndef MainApplicationComponent_hpp
#define MainApplicationComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PacketUI.hpp"
#include "AudioControls.h"
#include "GraphComponent.hpp"

/**
    The main application UI. Contains and positions all the UI classes.
 */
class MainApplicationComponent   : public Component
{
public:
    /**
        Constructor.
        @param audio_ Reference to the Audio class @see Audio
        @param packetSniffer_ Reference to the PacketSniffer class @see PacketSniffer
        @param packetParser_ Reference to the PacketParser class @see PacketParser
     */
    MainApplicationComponent (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_, PacketGraphicsComponent& graphicsComp_);
    
    /**
        Destructor.
     */
    ~MainApplicationComponent();
    
    /**
        Inherited from Component.
     */
    void paint (Graphics&) override;
    
    /**
         Inherited from Component.
     */
    void resized() override;
    
    /**
        Returns a reference to graph component
        @return The graph component @see GraphComponent
     */
    GraphComponent& getGraphComp () { return graphComp; };
    
    /**
        Sets the state of the component, called when the selected tab changes
        @param isActive True if this component's tab is selected, false if not
     */
    void setActive (bool isActive);
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainApplicationComponent)
    
    PacketUI packetUI;               /// Packet UI
    AudioControls audioControls;     /// Audio controls UI
    GraphComponent graphComp;        /// The graph component
};
#endif /* MainApplicationComponent_hpp */
