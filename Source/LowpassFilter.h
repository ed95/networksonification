/*
  ==============================================================================

    LowpassFilter.h
    Created: 18 Dec 2015 1:54:34pm
    Author:  Edward Davies

  ==============================================================================
*/

#ifndef LOWPASSFILTER_H_INCLUDED
#define LOWPASSFILTER_H_INCLUDED

#include "stdio.h"
#include "BaseFilter.h"

/** 
    A simple lowpass filter.
 */
class LowpassFilter : public BaseFilter
{
public:
    /**
        Constructor.
        @param audio_ A reference to the Audio class. @see Audio
     */
    LowpassFilter (Audio& audio_);
    
    /**
        Destructor.
     */
    ~LowpassFilter();
    
    /**
        Sets the filter cutoff. Inherited from BaseFilter @see BaseFilter.
        @param frequency The cutoff frequency
     */
    void setCutoff (float frequency) override;
    
private:
};


#endif  // LOWPASSFILTER_H_INCLUDED
