//
//  TextComponent.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 07/03/2016.
//
//

#include "TextComponent.hpp"

TextComponent::TextComponent() : shouldPrintText (false)
{
    addAndMakeVisible (&textOut);
    textOut.setMultiLine (true);
    textOut.setReadOnly (true);
    textOut.setFont (Font (Font::getDefaultMonospacedFontName(), 10.0f, Font::plain));
    
    printQueue.clear();
}

TextComponent::~TextComponent()
{
    
}

void TextComponent::resized()
{
    textOut.setBounds (getBounds());
}

void TextComponent::paint (Graphics &g)
{
    
}

void TextComponent::addToQueue (String str)
{
    printQueue.add (str);
}

void TextComponent::setActive (bool state)
{
    shouldPrintText = state;
    if (state)
    {
        startTimer (150);
    }
    else
    {
        if (isTimerRunning())
            stopTimer();
        
        printQueue.clear();
    }
}

bool TextComponent::isActive()
{
    return shouldPrintText;
}

void TextComponent::reset()
{
    textOut.clear();
    printQueue.clear();
}

void TextComponent::timerCallback()
{
    if (printQueue.size() != 0)
    {
        textOut.insertTextAtCaret (printQueue.remove (0));
        textOut.insertTextAtCaret (newLine);
        
        //skip over queue if we get too far behind
        if (printQueue.size() > 100)
            printQueue.clear();
    }
}