//
//  Audio.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#include "Audio.hpp"
#include "SineOscillator.hpp"
#include "SawOscillator.hpp"
#include "SquareOscillator.hpp"
#include "TriangleOscillator.hpp"

Audio::Audio()
{
    floatParams.clear();
    intParams.clear();
    
    // parameter initialisation
    sampleRate = new FloatParameter (defaultSampleRate, Parameters::pSampleRate, 1.f);
    masterLevel = new FloatParameter (defaultMasterLevel, Parameters::pMasterLevel, 0.02);
    floatParams.add (sampleRate);
    floatParams.add (masterLevel);
    
    for (int i = 0; i < 4; ++i)
    {
        aAttack[i] = new FloatParameter (defaultAmpAttack, (Parameters)(pAAttack0 + i), 0.01);
        floatParams.add (aAttack[i]);
    }
    
    aDecay = new FloatParameter (defaultAmpDecay, Parameters::pADecay, 0.01);
    aSustain = new FloatParameter (defaultAmpSustain, Parameters::pASustain, 0.01);
    floatParams.add (aDecay);
    floatParams.add (aSustain);
    
    for (int i = 0; i < 4; ++i)
    {
        aRelease[i] = new FloatParameter (defaultAmpRelease, (Parameters)(pARelease0 + i), 0.01);
        floatParams.add (aRelease[i]);
    }
    
    fAttack = new FloatParameter (defaultFilterAttack, Parameters::pFAttack, 0.01);
    fDecay = new FloatParameter (defaultFilterDecay, Parameters::pFDecay, 0.01);
    fSustain = new FloatParameter (defaultFilterSustain, Parameters::pFSustain, 0.01);
    fRelease = new FloatParameter (defaultFilterRelease, Parameters::pFRelease, 0.01);
    filterQ = new FloatParameter (defaultFilterQ, Parameters::pFilterQ, 0.1);
    floatParams.add (fAttack);
    floatParams.add (fDecay);
    floatParams.add (fSustain);
    floatParams.add (fRelease);
    floatParams.add (filterQ);
    
    for (int i = 0; i < 4; ++i)
    {
        oscillatorLevel[i] = new FloatParameter (defaultOscillatorLevel, (Parameters)(pOscillatorLevel0 + i), 0.01);
        floatParams.add (oscillatorLevel[i]);
    }
    
    reverbRoomSize = new FloatParameter (defaultReverbRoomSize, Parameters::pReverbRoomSize, 0.05);
    reverbWidth = new FloatParameter (defaultReverbWidth, Parameters::pReverbWidth, 0.05);
    floatParams.add (reverbRoomSize);
    floatParams.add (reverbWidth);
    
    intOffset = floatParams.size();
    
    filterCutoff = new IntegerParameter (defaultFilterCutoff, 20, 20000, Parameters::pFilterCutoff, 0.05);
    filterType = new IntegerParameter (defaultFilterType, 0, 2, Parameters::pFilterType, 1);
    intParams.add (filterCutoff);
    intParams.add (filterType);
    
    ARPChannel = new IntegerParameter (defaultARPChannel, 0, 3, Parameters::pARPChannel, 1);
    TCPChannel = new IntegerParameter (defaultTCPChannel, 0, 3, Parameters::pTCPChannel, 1);
    UDPChannel = new IntegerParameter (defaultUDPChannel, 0, 3, Parameters::pUDPChannel, 1);
    ICMPChannel = new IntegerParameter (defaultICMPChannel, 0, 3, Parameters::pICMPChannel, 1);
    
    intParams.add (ARPChannel);
    intParams.add (TCPChannel);
    intParams.add (UDPChannel);
    intParams.add (ICMPChannel);
    
    // audio device initialisation
    audioDeviceManager.initialiseWithDefaultDevices (0, 2); //0 inputs, 2 outputs
    audioDeviceManager.addAudioCallback (this);
    
    synthesiser.clearVoices();
    synthesiser.clearSounds();
    
    for (int i = 0; i < 10; i++)
    {
        synthesiser.addVoice (new SineVoice (*this));
        synthesiser.addVoice (new SawVoice (*this));
        synthesiser.addVoice (new SquareVoice (*this));
        synthesiser.addVoice (new TriangleVoice (*this));
    }
    synthesiser.addSound (new SineSound());
    synthesiser.addSound (new SawSound());
    synthesiser.addSound (new SquareSound());
    synthesiser.addSound (new TriangleSound());
    
    synthesiser.setNoteStealingEnabled (false);
    
    for (int i = 0; i < 4; ++i)
    {
        currentNotes[i].clear();
        noteTriggerLocks[i] = false;
        noteLength[i] = 200;
        largestPayloadSize[i] = 0;
        payloadSampleCount[i] = 0;
    }
    
    payloadSampleSize[ARPPACKET] = 10;
    payloadSampleSize[TCPPACKET] = 20;
    payloadSampleSize[UDPPACKET] = 20;
    payloadSampleSize[ICMPPACKET] = 5;
    
    // weightings for ARP note number selection
    int weightingA = 20;
    int weightingB = 5;
    for (int i = 0; i < 8; ++i)
    {
        if (i < 3)
        {
            arpWeights[0][i] = weightingA;
            arpWeights[1][i] = weightingB;
        }
        else
        {
            weightingA -= 3;
            weightingB += 3;
            arpWeights[0][i] = weightingA;
            arpWeights[1][i] = weightingB;
        }
    }
    
    reverb.setRoomSize (0.f);
    reverb.setWidth (0.f);
    reverb.setEffectMix (1.f);
    
    random.setSeed (Time::getCurrentTime().toMilliseconds());
    
    scaleToUse = constructRandomScale();
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
}

void Audio::audioDeviceIOCallback (const float **inputChannelData, int numInputChannels,
                                  float **outputChannelData, int numOutputChannels, int numSamples)
{
    updateParamatersFromQueue();
    
    AudioSampleBuffer sampleBuffer = AudioSampleBuffer (outputChannelData, numOutputChannels, numSamples);
    sampleBuffer.clear();
    
    MidiBuffer midiMessages;
    mmCollector.removeNextBlockOfMessages (midiMessages, numSamples);
    
    synthesiser.renderNextBlock (sampleBuffer, midiMessages, 0, numSamples);
    
    float roomSize = reverbRoomSize->getValue();
    float width = reverbWidth->getValue();
    reverb.setRoomSize(roomSize);
    reverb.setWidth (width);
    for(int i = 0; i < numSamples; i++)
    {
        reverb.tick (sampleBuffer.getSample(0, i), sampleBuffer.getSample (1, i), 0);
        const stk::StkFrames& samples = reverb.lastFrame();
        
        const float outputL = samples[0];
        const float outputR = samples[1];
        
        sampleBuffer.setSample (0, i, outputL);
        sampleBuffer.setSample (1, i, outputR);
    }
    
    for (int i = 0; i < numOutputChannels; i++)
        sampleBuffer.applyGain (i, 0, numSamples, masterLevel->getValue());
}

void Audio::audioDeviceAboutToStart (AudioIODevice *device)
{
    sampleRate->setValue (device->getCurrentSampleRate());
    
    mmCollector.reset (sampleRate->getValue());
    synthesiser.setCurrentPlaybackSampleRate (sampleRate->getValue());
    reverb.setSampleRate (sampleRate->getValue());
}

void Audio::audioDeviceStopped()
{
    if (isTimerRunning (TCPTIMERID))
        stopTimer (TCPTIMERID);
}

void Audio::timerCallback(int timerID)
{
    if (timerID == TCPTIMERID)
    {
        for (int i = 0; i < TCPNotes.size(); ++i)
        {
            TCPNote temp = TCPNotes.get (i);
            temp.rate = temp.currentCount / 10.f;
            if (temp.rate > 15)
                temp.rate = 15;
            temp.currentCount = 0;
            TCPNotes.set (i, temp);
        }
    }
    else if (timerID < 4)
    {
        noteTriggerLocks[timerID] = false;
        stopTimer (timerID);
    }
    else if (timerID < (127 + 1024))
    {
        synthesiser.noteOff (ARPChannel->getValue(), timerID - 1024, 127, true);
        currentNotes[ARPChannel->getValue()].removeAllInstancesOf (timerID - 1024);
        stopTimer (timerID);
    }
    else if (timerID < (127 + (2 * 1024)))
    {
        synthesiser.noteOff (TCPChannel->getValue(), timerID - (2 * 1024), 127, true);
        currentNotes[TCPChannel->getValue()].removeAllInstancesOf (timerID - (2 * 1024));
        stopTimer (timerID);
    }
    else if (timerID < (127 + (3 * 1024)))
    {
        synthesiser.noteOff (UDPChannel->getValue(), timerID - (3 * 1024), 127, true);
        currentNotes[UDPChannel->getValue()].removeAllInstancesOf (timerID - (3 * 1024));
        stopTimer (timerID);
    }
    else if (timerID < (127 + (4 * 1024)))
    {
        synthesiser.noteOff (ICMPChannel->getValue(), timerID - (4 * 1024), 127, true);
        currentNotes[ICMPChannel->getValue()].removeAllInstancesOf (timerID - (4 * 1024));
        stopTimer (timerID);
    }
}

void Audio::triggerNoteWithData (PacketData packet)
{
    if (packet.TCPDisconnection)
    {
        for (int i = 0; i < TCPNotes.size(); ++i)
        {
            TCPNote n = TCPNotes.get (i);
            if (packet.TCPID == n.ID)
            {
                synthesiser.noteOff (TCPChannel->getValue(), n.noteNum, 127, true);
                TCPNotes.remove (i);
                break;
            }
        }
    }
    else if (packet.TCPConnectionPacket)
    {
        for (int i = 0; i < TCPNotes.size(); ++i)
        {
            TCPNote temp = TCPNotes.get (i);
            if (temp.ID == packet.TCPID)
            {
                ++temp.currentCount;
                TCPNotes.set (i, temp);
                break;
            }
        }
    }
    else
    {
        if (packet.packetType == ARPPACKET)
        {
            int noteNum = noteMap (packet.ARPOp, ARPPACKET);
            
            synthesiser.noteOn (ARPChannel->getValue(), noteNum, rand() % 127);
            currentNotes[ARPChannel->getValue()].add (noteNum);
            startTimer (noteNum + (1 * 1024), noteLength[ARPPACKET]);
        }
        else if (packet.packetType == TCPPACKET)
        {
            int noteNum = noteMap (packet.payloadSize, TCPPACKET);
            int velocity = velocityMapTTL (packet.ttl);
            
            if (packet.TCPConnection)
            {
                synthesiser.noteOn (TCPChannel->getValue(), noteNum, velocity / 3);
                
                TCPNote note;
                note.noteNum = noteNum;
                note.ID = packet.TCPID;
                note.rate = 0;
                note.currentCount = 0;
                
                TCPNotes.insertAtHead (note);
            }
            else
            {
                synthesiser.noteOn (TCPChannel->getValue(), noteNum, velocity);
                currentNotes[TCPChannel->getValue()].add (noteNum);
                startTimer (noteNum + (2 * 1024), noteLength[TCPPACKET]);
            }
        }
        else if (packet.packetType == UDPPACKET)
        {
            int noteNum = noteMap (packet.payloadSize, UDPPACKET);
            int velocity = velocityMapTTL (packet.ttl);
            
            synthesiser.noteOn (UDPChannel->getValue(), noteNum, velocity);
            currentNotes[UDPChannel->getValue()].add (noteNum);
            startTimer (noteNum + (3 * 1024), noteLength[UDPPACKET]);
        }
        else if (packet.packetType == ICMPPACKET)
        {
            int noteNum = noteMap (packet.payloadSize, ICMPPACKET);
            int velocity = velocityMapTTL (packet.ttl);

            synthesiser.noteOn (ICMPChannel->getValue(), noteNum, velocity);
            currentNotes[ICMPChannel->getValue()].add (noteNum);
            startTimer (noteNum + (4 * 1024), noteLength[ICMPPACKET]);
        }
    }
    
}

void Audio::stopAllNotes()
{
    for (int i = 0; i < 4; ++i)
    {
        currentNotes[i].clear();
        synthesiser.allNotesOff (i, false);
    }
}

void Audio::updateParamatersFromQueue()
{
    while (audioParametersQueue.getNumReady())
    {
        ParameterPair param;
        audioParametersQueue.readFromFifo (&param, 1);
        
        if (param.t == ParameterPair::Float)
        {
            floatParams[param.identifier]->setValue (param.fVal);
        }
        else if (param.t == ParameterPair::Int)
        {
            intParams[param.identifier - intOffset]->setValue (param.iVal);
        }
        else if (param.t == ParameterPair::Bool)
        {
            
        }
        else if (param.t == ParameterPair::Flag)
        {
            if (param.identifier == Parameters::pStopAllNotes)
            {
                stopAllNotes();
            }
            else if (param.identifier == Parameters::pResetPayloadSize)
            {
                for (int i = 0; i < 4; ++i)
                {
                    largestPayloadSize[i] = 0;
                    payloadSampleCount[i] = 0;
                }
            }
        }
    }
    
    while (networkDataQueue.getNumReady())
    {
        DataToBeMapped data;
        networkDataQueue.readFromFifo (&data, 1);
        
        mapDataToParameter (data);
    }
}

int Audio::velocityMapTTL (int ttl)
{
    int vel;
    if (ttl != -1)
    {
        vel = 127 * ((float)ttl / 255.f);
        if (vel > 127)
            vel = 127;
    }
    else
    {
        vel = 127;
    }
    
    return vel;
}

int Audio::noteMap (int data, int protocol)
{
    if (protocol == ARPPACKET)
    {
        int noteIndex;
        int arpProtocol;
        
        if (data == ARP_REQUEST || data == ARP_REPLY) // weight notes towards lower end of scale
            arpProtocol = 0;
        else if (data == RARP_REQUEST || data == RARP_REPLY) // weight notes towards higher end of scale
            arpProtocol = 1;
        
        // generate note from scale based on weightings
        int weightSum = 0;
        for (int i = 0; i < 8; ++i)
            weightSum += arpWeights[arpProtocol][i];
        
        int rdm = random.nextInt (weightSum);
        for (int i = 0; i < 8; ++i)
        {
            if (rdm < arpWeights[arpProtocol][i])
            {
                noteIndex = i;
                break;
            }
            
            rdm -= arpWeights[arpProtocol][i];
        }
        
        int noteNum = scaleToUse[noteIndex];
        
        if (data == ARP_REPLY || data == RARP_REPLY)
        {
            noteNum += 12;
        }
        
        return noteNum;
    }
    else
    {
        int noteNum = scaleToUse[random.nextInt (8)];
        
        if (data > 65535)
            data = 65535;
        
        if (data >largestPayloadSize[protocol])
            largestPayloadSize[protocol] = data;
        
        if (payloadSampleCount[protocol] >= payloadSampleSize[protocol])
        {
            float prop = (float)data / largestPayloadSize[protocol];
            int offset;
            
            if (prop < 0.3)
                offset = 0;
            else if (prop < 0.6)
                offset = -1;
            else
                offset = -2;
            
            noteNum = noteNum + (offset * 12);
        }
        else
        {
            ++payloadSampleCount[protocol];
        }
        
        return noteNum;
    }
}

Array<int> Audio::constructRandomScale()
{
    Array<int> scale;
    Range<int> noteRange = Range<int> (57, 69);
    
    int rootNote = random.nextInt (noteRange);
    int scaleType = random.nextInt (1);

    if (scaleType == 0) // major
    {
        scale.add (rootNote);
        for (int i = 0; i < 7; ++i)
        {
            if (i == 2 || i == 6)
                rootNote += 1;
            else
                rootNote += 2;
            
            scale.add (rootNote);
        }
    }
    else if (scaleType == 1) //minor
    {
        scale.add (rootNote);
        for (int i = 0; i < 7; ++i)
        {
            if (i == 1 || i == 4)
                rootNote += 1;
            else if (i == 5)
                rootNote += 3;
            else
                rootNote += 2;
            
            scale.add (rootNote);
        }
    }

    return scale;
}

TCPNote Audio::getTCPNote (int noteToGet)
{
    TCPNote temp;
    for (int i = 0; i < TCPNotes.size(); ++i)
    {
        temp = TCPNotes.get (i);
        if (temp.noteNum == noteToGet)
        {
            return temp;
        }
    }
    
    temp.ID = -1;
    return temp;
}

void Audio::mapDataToParameter (DataToBeMapped data)
{
    switch (data.identifier)
    {
        case ARP:
            if (!noteTriggerLocks[ARPPACKET])
            {
                triggerNoteWithData (data.pData);
                noteTriggerLocks[ARPPACKET] = true;
                startTimer (ARPPACKET, noteLength[ARPPACKET] / 4);
            }
            break;
        case TCP:
            if (data.pData.TCPConnection || data.pData.TCPDisconnection || data.pData.TCPConnectionPacket)
            {
                triggerNoteWithData (data.pData);
            }
            else if (!noteTriggerLocks[TCPPACKET])
            {
                triggerNoteWithData (data.pData);
                noteTriggerLocks[TCPPACKET] = true;
                startTimer (TCPPACKET, noteLength[TCPPACKET] / 4);
            }
            break;
        case UDP:
            if (!noteTriggerLocks[UDPPACKET])
            {
                triggerNoteWithData (data.pData);
                noteTriggerLocks[UDPPACKET] = true;
                startTimer (UDPPACKET, noteLength[UDPPACKET] / 4);
            }
            break;
        case ICMP:
            if (!noteTriggerLocks[UDPPACKET])
            {
                triggerNoteWithData (data.pData);
                noteTriggerLocks[UDPPACKET] = true;
                startTimer (UDPPACKET, noteLength[UDPPACKET] / 4);
            }
            break;
        case packetsPerSecond:
            reverbRoomSize->setValue (linearMap (0, 200, 0, 1, data.fVal));
            break;
        case ppsARP:
            aAttack[ARPPACKET]->setValue (linearMap (0, 10, 0.2, 0.05, data.fVal));
            aRelease[ARPPACKET]->setValue (linearMap (0, 10, 0.5, 0.01, data.fVal));
            noteLength[ARPPACKET] = linearMap (0, 10, 300, 100, data.fVal);
            break;
        case ppsTCP:
            aAttack[TCPPACKET]->setValue (linearMap (0, 50, 0.2, 0.05, data.fVal));
            aRelease[TCPPACKET]->setValue (linearMap (0, 50, 0.5, 0.01, data.fVal));
            noteLength[TCPPACKET] = linearMap (0, 50, 300, 100, data.fVal);
            break;
        case ppsUDP:
            aAttack[UDPPACKET]->setValue (linearMap (0, 50, 0.2, 0.05, data.fVal));
            aRelease[UDPPACKET]->setValue (linearMap (0, 50, 0.5, 0.01, data.fVal));
            noteLength[UDPPACKET] = linearMap (0, 50, 300, 100, data.fVal);
            break;
        case ppsICMP:
            aAttack[ICMPPACKET]->setValue (linearMap (0, 10, 0.2, 0.05, data.fVal));
            aRelease[ICMPPACKET]->setValue (linearMap (0, 10, 0.5, 0.01, data.fVal));
            noteLength[ICMPPACKET] = linearMap (0, 10, 300, 100, data.fVal);
            break;
        case numIPConnections:
            reverbWidth->setValue (linearMap (0, 20, 0, 1, data.iVal));
            break;
        default:
            break;
    }

}

float Audio::linearMap(float inMin, float inMax, float outMin, float outMax, float input)
{
    if (input < inMin)
        input = inMin;
    else if (input > inMax)
        input = inMax;
        
    return (outMin + ((input - inMin) / (inMax - inMin) * (outMax - outMin)));
}