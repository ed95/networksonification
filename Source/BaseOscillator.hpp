//
//  BaseOscillator.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 11/12/2015.
//
//

#ifndef BaseOscillator_hpp
#define BaseOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "LowpassFilter.h"

/**
 Base class for oscillators
 */

class BaseOscillator : public SynthesiserVoice
{
public:
    /** 
        Constructor.
        @param audio_ A reference to the Audio class @see Audio
        @param oscNum_ This oscillator's waveshape
     */
    BaseOscillator (Audio& audio_, int waveshape_);
    
    /** 
        Destructor.
     */
    ~BaseOscillator();
    
    /** 
        Inherited from SynthesiserVoice. 
     */
    virtual bool canPlaySound (SynthesiserSound* sound) override = 0;
    
    /**
        Inherited from SynthesiserVoice.
     */
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* sound,
                    int currentPitchWheelPosition) override;
    
    /**
        Inherited from SynthesiserVoice.
     */
    void stopNote (float velocity, bool allowTailOff) override;
    
    /**
        Inherited from SynthesiserVoice.
     */
    void pitchWheelMoved (int newValue) override;
    
    /**
        Inherited from SynthesiserVoice.
     */
    void controllerMoved (int controllerNumber, int newValue) override;
    
    /**
        Inherited from SynthesiserVoice. Renders the next buffer of audio
     */
    void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) override;
    
    /** 
        Virtual function for oscillators to inherit and render unique waveshapes.
        @return The unique waveshape
     */
    virtual float renderWaveshape() = 0;
    
    /**
        Virtual function which sets the frequency of the STK generator.
        @param frequency The frequency to set the STK generator
     */
    virtual void setSTKFreq (float frequency) = 0;
    
    /** 
        Virtual function which resets the STK generator. 
     */
    virtual void resetSTKGen() = 0;
    
    /**
         Virtual function that sets whether vibrato should be on or off for a voice.
         @param isOn True if vibrato should be on, false if off
     */
    virtual void vibratoOn (bool isOn) = 0;
    
    /**
         Virtual function that sets the rate and depth of the vibrato effect for a voice.
         @param vibRate The rate in Hz of the vibrato LFO
         @param vibDepth The relative depth of the effect between 0 and 1
     */
    virtual void setVibRateDepth (float vibRate, float vibDepth) = 0;
    
    /** 
        Called each block to update all the parameters from the Audio class. 
     */
    void updateParameters();
    
    /** Eliminate very small or very large numbers. Adapted from the SuperCollider source code.
     @param x The input to be tested
     @return The result of the test. 0 if smaller than 1e-15 or larger than 1e15, unaltered if not
     */
    inline float zapGremlins (float x)
    {
        float absx = std::abs (x);
        return (absx > (float)1e-15 && absx < (float)1e15) ? x : (float)0.;
    }
    
private:
    Audio& audio;                /// Reference to the Audio class
    stk::ADSR amplitudeEnvelope; /// STK amplitude envelope
    LowpassFilter lpf;           /// Lowpass filter
    
    int waveshape;               /// Current waveshape
    float amplitude;             /// Amplitude of the note based on velocity
    float frequency;             /// Frequency of the oscillator
    int filterCutoff;            /// Filter cutoff
    float level;                 /// Level of the oscillator
    
    float vibDepth;              /// The relative depth of the vibrato effect
    float vibRate;               /// The rate in Hz of the vibrato LFO
};

#endif /* BaseOscillator_hpp */
