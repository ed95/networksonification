//
//  GraphicsSettingsComponent.hpp
//  NetworkSonification
//
//  Created by Edward Davies on 16/03/2016.
//
//

#ifndef GraphicsSettingsComponent_hpp
#define GraphicsSettingsComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PacketGraphicsComponent.hpp"
#include "PacketParser.hpp"

/**
    Button class for changing the packet graphics settings
 */
class EllipseButton : public TextButton
{
public:
   
    /**
        Constructor.
     */
    EllipseButton (){};
    
    /**
         Destructor.
     */
    ~EllipseButton(){};
    
    /**
        Sets the protcol type for this button.
     */
    void setProtocolType (int protocol)
    {
        switch (protocol)
        {
            case ARPPACKET:
                colour = Colours::maroon;
                protocolLabel = "ARP";
                break;
            case TCPPACKET:
                colour = Colours::midnightblue;
                protocolLabel = "TCP";
                break;
            case UDPPACKET:
                colour = Colours::darkgreen;
                protocolLabel = "UDP";
                break;
            case ICMPPACKET:
                colour = Colours::white;
                protocolLabel = "ICMP";
                break;
        };
    };
    
    /**
         Inherited from component.
     */
    void paint (Graphics& g)
    {
        g.setColour (colour);
        g.fillEllipse (2.0f, 2.0f, getWidth() - 4.0f, getHeight() - 4.0f);
        
        g.setColour (Colours::black);
        g.drawEllipse (2.0f, 2.0f, getWidth() - 4.0f, getHeight() - 4.0f, 1.0f);
        
        g.setColour (colour.contrasting (1.f));
        g.drawText(protocolLabel, 2.f, 2.f, getWidth() - 4.f, getHeight() - 4.f, Justification::centred);
    };
    
    /**
        Changes the button to a new colour.
     */
    void newColour (Colour newColour)
    {
        colour = newColour;
        repaint();
    };
    
    
private:
    Colour colour;        /// The colour for this button
    String protocolLabel; /// Label displaying the protocol
};

/**
    Graphics settings component.
 */
class GraphicsSettingsComponent : public Component,
                                  public MultiTimer,
                                  public Slider::Listener,
                                  public Button::Listener,
                                  public ChangeListener
{
public:
    /**
        Constructor.
        @param parser_ Reference to the PacketParser class @see PacketParser
        @param graphicsComp_ Reference to the PacketGraphicsComponent @see PacketGraphicsComponent
     */
    GraphicsSettingsComponent (PacketParser& parser_, PacketGraphicsComponent& graphicsComp_);
    
    /**
        Destructor.
     */
    ~GraphicsSettingsComponent();
    
    /**
        Inherited from Component.
     */
    void resized() override;
    
    /**
         Inherited from Component.
     */
    void paint (Graphics& g) override;
    
    /**
         Inherited from MultiTimer.
     */
    void timerCallback(int timerID) override;
    
    /**
         Inherited from Slider::Listener.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /**
         Inherited from Button::Listener.
     */
    void buttonClicked (Button* button) override;
    
    /**
        Inherited from ChangeListener.
     */
    void changeListenerCallback (ChangeBroadcaster* source) override;
    
private:
    PacketParser& parser;                  /// Reference to the PacketParser class
    PacketGraphicsComponent& graphicsComp; /// Reference to the PacketGraphicsComponent class
    
    Slider sampleIntervalSlider; /// Slider to set the packet sample interval
    Label sampleIntervalLabel;   /// Label for the sample interval slider
    
    TooltipWindow tt;                    /// Tooltip
    Image infoTooltipImage;              /// Image for info tooltip
    ImageComponent infoTooltipImageComp; /// Image component for info tooltip
    
    int itemHeight; /// Height for UI items
    
    EllipseButton protocolColourButton[4];    /// Buttons for each protocol
    ColourSelector protocolColourSelector[4]; /// Colour selector for each protocol
    int selectedButton;                       /// Stores the button that has been selected
    
    bool buttonClickedFlag[4]; /// Used for detecting double clicks
    
    Colour defaultColours[4]; /// The default colours for the protocols
};

#endif /* GraphicsSettingsComponent_hpp */
