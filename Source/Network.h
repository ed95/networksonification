//
//  Network.h
//  NetworkSonification
//
//  Created by Edward Davies on 01/12/2015.
//
//

#ifndef Network_h
#define Network_h

#include <pcap.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <sys/time.h>

#define SNAP_LEN 1518       /// maximum bytes per packet to capture
#define SIZE_ETHERNET 14    /// ethernet header size in bytes
#define ETHER_ADDR_LEN	6   /// ethernet address length in bytes

/**
    Ethernet header
 */
struct sniffEthernet {
    u_char ether_dHost[ETHER_ADDR_LEN]; /// MAC host
    u_char ether_sHost[ETHER_ADDR_LEN]; /// Mac source
    u_short ether_type;                 /// IP, ARP, RARP etc.
};

/**
     IP header
 */
struct sniffIP {
    u_char IP_vhl;                  /// version << 4 | header length >> 2
    u_char IP_tos;                  /// type of service
    u_short IP_len;                 /// total length
    u_short IP_id;                  /// identification
    u_short IP_off;                 /// fragment offset field
#define IP_RF 0x8000                /// reserved fragment flag
#define IP_DF 0x4000                /// dont fragment flag
#define IP_MF 0x2000                /// more fragments flag
#define IP_OFFMASK 0x1fff           /// mask for fragmenting bits
    u_char IP_ttl;                  /// time to live
    u_char IP_p;                    /// protocol
    u_short IP_sum;                 /// checksum
    struct in_addr IP_src, IP_dst;  /// source and dest address
};
#define IP_HL(ip)		(((ip)->IP_vhl) & 0x0f)
#define IP_V(ip)		(((ip)->IP_vhl) >> 4)


typedef u_int TCP_seq;

/**
     TCP header
 */
struct sniffTCP {
    u_short TCP_sPort;	/// source port
    u_short TCP_dPort;	/// destination port
    u_int TCP_seq;		/// sequence number
    u_int TCP_ack;		/// acknowledgement number
    u_char TCP_offx2;	/// data offset, rsvd
#define TCP_OFF(TCP)	(((TCP)->TCP_offx2 & 0xf0) >> 4)
    u_char TCP_flags;   /// TCP flags
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
    u_short TCP_win;	/// window
    u_short TCP_sum;	/// checksum
    u_short TCP_urp;	/// urgent pointer
};

/**
     UDP header
 */
struct sniffUDP {
    u_short UDP_sPort; /// source port
    u_short UDP_dPort; /// destination port
    u_short UDP_len;   /// length
    u_short UDP_sum;   /// checksum
};

/**
     ICMP header
 */
struct sniffICMP {
    u_char ICMP_type;
    u_char ICMP_code;
    u_short ICMP_sum;
    u_int32_t ICMP_roh; /// rest of header
};

/**
     ARP header
 */
struct sniffARP {
    u_int16_t ARP_hType;    /// hardware type
    u_int16_t ARP_pType;    /// protocol type
    u_char ARP_hLen;        /// hardware address length
    u_char ARP_pLen;        /// protocol address length
    u_int16_t ARP_oper;     /// operation code
    u_char ARP_sHa[6];      /// sender hardware address
    struct in_addr ARP_sIp; /// sender IP address
    u_char ARP_tHa[6];      /// target hardware address
    struct in_addr ARP_tIp; /// target IP address
};
#define ARP_REQUEST 1   /// ARP request
#define ARP_REPLY 2     /// ARP reply
#define RARP_REQUEST 3  /// RARP request
#define RARP_REPLY 4    /// RARP reply

/// error defines
#define ERR_NODEVICES 1
#define ERR_NETMASK 2
#define ERR_DEVICEOPEN 3
#define ERR_NOTETHERNET 4
#define ERR_FILTERPARSE 5
#define ERR_FILTERINSTALL 6

/// packet protocol defines
#define ARPPACKET 0
#define TCPPACKET 1
#define UDPPACKET 2
#define ICMPPACKET 3

/**
     Struct containing sniffer info
 */
struct NetworkInfo
{
    const char* currentDevice;
    const char* currentNet;
    const char* currentMask;
    const char* filterExpression;
    bool promiscuousMode;
};

/**
     Struct containing packet data
 */
struct PacketData
{
    int packetType;
    int ttl;
    int payloadSize;
    int ARPOp;
    int ICMPType;
    int TCPID;
    bool TCPConnection;
    bool TCPDisconnection;
    bool TCPConnectionPacket;
};

/**
     Struct containing an IP address and its frequency during the capture session
 */
struct IP
{
    IPAddress address;
    int freq;
};

/**
     Struct for monitoring IP connections
 */
struct IPConnection
{
    IPAddress source;
    IPAddress destination;
    
    bool operator == (const IPConnection &rhs)
    {
        if ((source == rhs.source && destination == rhs.destination) || (source == rhs.destination && destination == rhs.source))
            return true;
        
        return false;
    }
};

/**
    Struct for monitoring TCP connections
 */
struct TCPConnection
{
    IPAddress sourceIP;
    IPAddress destIP;
    int sourcePort;
    int destPort;
    int currentStage = 0;
    int ID;
    
    /**
        Compares two TCPConnection objects to see if they contain the same data
        @param rhs The other TCPConnection for comparison
        @return True if the source/destination IP and port numbers are the same, false if not
     */
    bool operator == (const TCPConnection &rhs)
    {
        if ((sourceIP == rhs.sourceIP) && (destIP == rhs.destIP) && (sourcePort == rhs.sourcePort) && (destPort == rhs.destPort))
            return true;
        
        return false;
    }
    
    /**
         Compares two TCPConnection objects to see if they contain the same data but inverted
         @param rhs The other TCPConnection for comparison
         @return True if the lhs source/dest IP and port numbers are the same as the rhs dest/source
     */
    bool isInverse (const TCPConnection &rhs)
    {
        if ((sourceIP == rhs.destIP) && (destIP == rhs.sourceIP) && (sourcePort == rhs.destPort) && (destPort == rhs.sourcePort))
            return true;
        
        return false;
    }
    
    /**
        Inverts a TCPConnection (client - server / server - client)
     */
    void operator ! ()
    {
        IPAddress temp;
        int tempP;
        
        temp = sourceIP;
        sourceIP = destIP;
        destIP = temp;
        
        tempP = sourcePort;
        sourcePort = destPort;
        destPort = tempP;
    }
};

/**
    Struct containing packets per second (PPS) data for each protocol and the total PPS
 */
struct PPSData
{
    float totalPPS;
    float arpPPS;
    float tcpPPS;
    float udpPPS;
    float icmpPPS;
};

#endif /* Network_h */
