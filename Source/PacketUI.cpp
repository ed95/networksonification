//
//  PacketUI.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 10/12/2015.
//
//

#include "PacketUI.hpp"

PacketUI::PacketUI (Audio& audio_, PacketSniffer& packetSniffer_, PacketParser& packetParser_, GraphComponent& graphComp_) : audio (audio_), packetSniffer (packetSniffer_), packetParser (packetParser_), graphComp (graphComp_), arpLabel ("", "ARP"), tcpLabel ("", "TCP"), udpLabel ("", "UDP"), icmpLabel ("", "ICMP"), arpCounter ("", "0"), tcpCounter ("", "0"), udpCounter ("", "0"), icmpCounter ("", "0"), arpPpsLabel ("", "ARP PPS: -"), tcpPpsLabel ("", "TCP PPS: -"), udpPpsLabel ("", "UDP PPS: -"), icmpPpsLabel ("", "ICMP PPS: -"), itemHeight (24)
{
    addAndMakeVisible (&arpLabel);
    arpLabel.setJustificationType (Justification::centred);
    addAndMakeVisible (&arpCounter);
    arpCounter.setJustificationType (Justification::centred);
    addAndMakeVisible (&arpPpsLabel);
    arpPpsLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (&tcpLabel);
    tcpLabel.setJustificationType (Justification::centred);
    addAndMakeVisible (&tcpCounter);
    tcpCounter.setJustificationType (Justification::centred);
    addAndMakeVisible (&tcpPpsLabel);
    tcpPpsLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (&udpLabel);
    udpLabel.setJustificationType (Justification::centred);
    addAndMakeVisible (&udpCounter);
    udpCounter.setJustificationType (Justification::centred);
    addAndMakeVisible (&udpPpsLabel);
    udpPpsLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (&icmpLabel);
    icmpLabel.setJustificationType (Justification::centred);
    addAndMakeVisible (&icmpCounter);
    icmpCounter.setJustificationType (Justification::centred);
    addAndMakeVisible (&icmpPpsLabel);
    icmpPpsLabel.setJustificationType (Justification::centred);
    
    File location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory()
    .getChildFile ("Resources").getChildFile ("resources");
    
    if (!location.exists())
    {
        location = location.getSpecialLocation (File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile ("resources");
    }
    
    File playImagePath = File(location.getChildFile ("images/play.png"));
    File stopImagePath = File(location.getChildFile ("images/stop.png"));
    
    playImage = ImageFileFormat::loadFrom (playImagePath);
    stopImage = ImageFileFormat::loadFrom (stopImagePath);
    
    startButton.setImages (false, true, false, playImage, 1.f, Colours::transparentWhite, Image(), 1.f,
                           Colours::transparentWhite, Image(), 0.5, Colours::transparentWhite, 0.f);
    stopButton.setImages (false, true, false, stopImage, 1.f, Colours::transparentWhite, Image(), 1.f,
                           Colours::transparentWhite, Image(), 0.5, Colours::transparentWhite, 0.f);
    
    addAndMakeVisible (&startButton);
    startButton.addListener (this);
    addAndMakeVisible (&stopButton);
    stopButton.addListener (this);
    
    stopButton.setEnabled (false);
    
    for (int i = 0; i < 4; ++i)
    {
        protocolWaveshape[i].addItem ("Triangle", 1);
        protocolWaveshape[i].addItem ("Sine", 2);
        protocolWaveshape[i].addItem ("Sawtooth", 3);
        protocolWaveshape[i].addItem ("Square", 4);
        
        addAndMakeVisible (protocolWaveshape[i]);
        protocolWaveshape[i].setSelectedId (i + 1);
        protocolWaveshape[i].addListener (this);
    }
    
    packetParser.setListener (this);
}

PacketUI::~PacketUI()
{
    
}

void PacketUI::paint(juce::Graphics &)
{
    
}

void PacketUI::resized()
{
    Rectangle<int> r (getBounds());
    const int space = itemHeight / 5;
    
    r.removeFromTop (space * 3);
    Rectangle<int> buttons = r.removeFromTop (itemHeight * 2);
    
    startButton.setBounds (buttons.withWidth (itemHeight * 2).withX ((getWidth() / 2) - (itemHeight * 2) - 10));
    stopButton.setBounds (buttons.withWidth (itemHeight * 2).withX (startButton.getRight() + 20));
    
    r.removeFromTop (space * 2);
    
    Rectangle<int> labels = r.removeFromTop (itemHeight).withWidth (proportionOfWidth (0.25));
    arpLabel.setBounds (labels.withX (0));
    tcpLabel.setBounds (labels.withX (proportionOfWidth (0.25)));
    udpLabel.setBounds (labels.withX (proportionOfWidth (0.5)));
    icmpLabel.setBounds (labels.withX (proportionOfWidth (0.75)));
    
    r.removeFromTop (space);
    
    labels = r.removeFromTop (itemHeight).withWidth (proportionOfWidth (0.25));
    arpCounter.setBounds (labels.withX (0));
    tcpCounter.setBounds (labels.withX (proportionOfWidth (0.25)));
    udpCounter.setBounds (labels.withX (proportionOfWidth (0.5)));
    icmpCounter.setBounds (labels.withX (proportionOfWidth (0.75)));
    
    r.removeFromTop (space);
    
    labels = r.removeFromTop (itemHeight).withWidth (proportionOfWidth (0.25));
    arpPpsLabel.setBounds (labels.withX (0));
    tcpPpsLabel.setBounds (labels.withX (proportionOfWidth (0.25)));
    udpPpsLabel.setBounds (labels.withX (proportionOfWidth (0.5)));
    icmpPpsLabel.setBounds (labels.withX (proportionOfWidth (0.75)));
    
    r.removeFromTop (space * 2);
    
    labels = r.removeFromTop (itemHeight * 1.25).withWidth (proportionOfWidth (0.25));
    for (int i = 0; i < 4; ++i)
    {
        protocolWaveshape[i].setBounds (labels.withX (proportionOfWidth(0 + (i * 0.25))));
    }
}

void PacketUI::buttonClicked (juce::Button *button)
{
    if (button == &startButton)
    {
        if (!packetSniffer.isSnifferInitialised())
        {
            AlertWindow::showMessageBox (AlertWindow::AlertIconType::WarningIcon, "Error", "Packet sniffer failed to initialise - change settings");
        }
        else
        {
            packetSniffer.setSnifferState (true);
            
            arpCounter.setText ("0", dontSendNotification);
            tcpCounter.setText ("0", dontSendNotification);
            udpCounter.setText ("0", dontSendNotification);
            icmpCounter.setText ("0", dontSendNotification);
            
            stopButton.setEnabled (true);
            startButton.setEnabled (false);
            graphComp.isSnifferRunning (true);
        }
    }
    else if (button == &stopButton)
    {
        packetSniffer.setSnifferState (false);
        startTimer (1, 1500); //wait for sniffer thread to exit
        stopButton.setEnabled (false);
        stopTimer (0);
        graphComp.isSnifferRunning (false);
    }
}

void PacketUI::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    for (int i = 0; i < 4; ++i)
    {
        if (comboBoxThatHasChanged == &protocolWaveshape[i])
        {
            ParameterPair param;
            param.identifier = Parameters::pStopAllNotes;
            param.t = ParameterPair::Flag;
            audio.audioParametersQueue.addToFifo (&param, 1);
            
            param.identifier = (Parameters)(pARPChannel + i);
            param.t = ParameterPair::Int;
            param.iVal = protocolWaveshape[i].getSelectedId() - 1;
            audio.audioParametersQueue.addToFifo (&param, 1);
        }
    }
}

void PacketUI::updateLabels (PPSData pps)
{
    MessageManagerLock mml (Thread::getCurrentThread());
    
    if (!mml.lockWasGained())
        return;
    
    arpPpsLabel.setText ("ARP PPS " + String (pps.arpPPS, 2), dontSendNotification);
    tcpPpsLabel.setText ("TCP PPS " + String (pps.tcpPPS, 2), dontSendNotification);
    udpPpsLabel.setText ("UDP PPS " + String (pps.udpPPS, 2), dontSendNotification);
    icmpPpsLabel.setText ("ICMP PPS " + String (pps.icmpPPS, 2), dontSendNotification);
    
    graphComp.newGraphData (pps);
}

void PacketUI::newPacket (int packetProtocol, int packetCount)
{
    MessageManagerLock mml (Thread::getCurrentThread());
    
    if (!mml.lockWasGained())
        return;
    
    switch (packetProtocol) {
        case ARPPACKET:
            arpCounter.setText (String (packetCount), dontSendNotification);
            break;
        case TCPPACKET:
            tcpCounter.setText (String (packetCount), dontSendNotification);
            break;
        case UDPPACKET:
            udpCounter.setText (String (packetCount), dontSendNotification);
            break;
        case ICMPPACKET:
            icmpCounter.setText (String (packetCount), dontSendNotification);
            break;
        default:
            break;
    }

}

void PacketUI::timerCallback(int timerID)
{
    if (timerID == 1)
    {
        startButton.setEnabled (true);
        stopTimer (1);
    }
}