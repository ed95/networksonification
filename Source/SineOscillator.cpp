//
//  SineOscillator.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 11/12/2015.
//
//

#include "SineOscillator.hpp"

SineVoice::SineVoice (Audio& audio_) : BaseOscillator (audio_, 1)
{
    isVibratoOn = false;
}

SineVoice::~SineVoice()
{
    
}

bool SineVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<SineSound*> (sound) != nullptr;
};

void SineVoice::setSTKFreq (float frequency)
{
    sineGen.setFrequency (frequency);
    baseFreq = frequency;
}

void SineVoice::resetSTKGen()
{
    sineGen.reset();
    vibGen.reset();
}

float SineVoice::renderWaveshape()
{
    if (isVibratoOn)
    {
        float relativeDepth = vDepth * baseFreq;
        sineGen.setFrequency (baseFreq + (vibGen.tick() * relativeDepth));
        return sineGen.tick();
    }
    else
    {
        return sineGen.tick();
    }
}

void SineVoice::vibratoOn (bool isOn)
{
    isVibratoOn = isOn;
}

void SineVoice::setVibRateDepth (float vibRate, float vibDepth)
{
    vibGen.setFrequency (vibRate);
    vDepth = vibDepth;
}