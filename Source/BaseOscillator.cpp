//
//  BaseOscillator.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 11/12/2015.
//
//

#include "BaseOscillator.hpp"
#include "Audio.hpp"
#include "LowpassFilter.h"

BaseOscillator::BaseOscillator (Audio& audio_, int waveshape_) : audio (audio_), lpf (audio_), waveshape (waveshape_)
{
    amplitude = 0.f;
    frequency = 440.f;
    vibDepth = 0.3;
    vibRate = 0;
}

BaseOscillator::~BaseOscillator()
{
    
}

void BaseOscillator::startNote (int midiNoteNumber, float velocity, SynthesiserSound *sound, int currentPitchWheelPosition)
{
    frequency = MidiMessage::getMidiNoteInHertz (midiNoteNumber);
    setSTKFreq (frequency);
    amplitude = velocity / 127.f;
    amplitudeEnvelope.keyOn();
}

void BaseOscillator::stopNote (float velocity, bool allowTailOff)
{
    amplitudeEnvelope.keyOff();
}

void BaseOscillator::pitchWheelMoved (int newValue)
{
    
}

void BaseOscillator::controllerMoved (int controllerNumber, int newValue)
{
    
}

void BaseOscillator::renderNextBlock (AudioSampleBuffer &outputBuffer, int startSample, int numSamples)
{
    updateParameters();
    
    while(--numSamples >= 0)
    {
        float envelopeOutput = amplitudeEnvelope.tick();
        
        float output = renderWaveshape() * amplitude * envelopeOutput * level;
        output = lpf.outputFiltered (output);

        for (int i = outputBuffer.getNumChannels(); --i >= 0;)
        {
            outputBuffer.addSample (i, startSample, output);
        }
        ++startSample;
        
        if (envelopeOutput == 0)
        {
            clearCurrentNote();
            resetSTKGen();
            break;
        }
        
    }
}

void BaseOscillator::updateParameters()
{
    level = audio.oscillatorLevel[waveshape]->getValue();
    amplitudeEnvelope.setAllTimes (audio.aAttack[waveshape]->getValue(), audio.aDecay->getValue(), audio.aSustain->getValue(), audio.aRelease[waveshape]->getValue());
    
    if (waveshape == audio.TCPChannel->getValue())
    {
        TCPNote note = audio.getTCPNote (getCurrentlyPlayingNote()); //check if this note is an active TCP connection
        if (note.ID != -1)
        {
            if (note.rate == 0)
            {
                vibratoOn (false);
            }
            else
            {
                vibratoOn (true);
                setVibRateDepth (note.rate, vibDepth);
            }
        }
    }
    
    filterCutoff = audio.filterCutoff->getValue();
    lpf.setCutoff (filterCutoff);
}