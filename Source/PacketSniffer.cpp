//
//  PacketSniffer.cpp
//  NetworkSonification
//
//  Created by Edward Davies on 01/12/2015.
//
//

#include "PacketSniffer.hpp"

PacketSniffer::PacketSniffer (PacketParser& parser_) : Thread ("PacketCap"), snifferState (false), parser (parser_), initialised (false)
{
    pcap_if_t* allDevs;
    char errbuf[PCAP_ERRBUF_SIZE];
    
    if (pcap_findalldevs (&allDevs, errbuf) == -1)
    {
        deviceNameArray.clear();
    }
    else
    {
        for (pcap_if_t* d = allDevs; d != nullptr; d = d->next)
        {
            deviceNameArray.add (d->name);
        }
    }
}

PacketSniffer::~PacketSniffer()
{
    stopThread (500);
}

int PacketSniffer::initialise (const char *captureDevice, const char *filterExpression, bool promiscMode, NetworkInfo* info)
{
    char* netString;
    char* maskString;
    struct in_addr addrStruct;
    dev = captureDevice;
    filter = filterExpression;
    
    initialised = false;
    
    if (pcap_lookupnet (dev, &net, &mask, errBuff) == -1)
    {
        return ERR_NETMASK;
    }
    
    addrStruct.s_addr = net;
    netString = inet_ntoa (addrStruct);
    
    addrStruct.s_addr = mask;
    maskString = inet_ntoa (addrStruct);
    
    info->currentDevice = captureDevice;
    info->currentNet = netString;
    info->currentMask = maskString;
    info->filterExpression = filter;
    info->promiscuousMode = promiscMode;
    
    
    int result = setPromiscuousMode (promiscMode);
    if (result != 0)
       return result;
    
    initialised = true;
    return 0;
}

void PacketSniffer::setSnifferState (bool state)
{
    if (state)
    {
        if( !isThreadRunning())
            startThread();
        
        parser.captureStarted();
    }
    else
    {
        if (isThreadRunning())
            signalThreadShouldExit();
        
        parser.captureStopped();
    }
    
}

bool PacketSniffer::getSnifferState()
{
    return isThreadRunning();
}

bool PacketSniffer::isSnifferInitialised()
{
    return initialised;
}

int PacketSniffer::setPromiscuousMode (bool on)
{    
    handle = pcap_open_live (dev, SNAP_LEN, on, 1000, errBuff);
    
    if (handle == NULL)
    {
        return ERR_DEVICEOPEN;
    }
    if (pcap_datalink(handle) != DLT_EN10MB)
    {
        return ERR_NOTETHERNET;
    }
    if (pcap_compile (handle, &fp, filter, 0, net) == -1)
    {
        return ERR_FILTERPARSE;
    }
    if (pcap_setfilter (handle, &fp) == -1)
    {
        return ERR_FILTERINSTALL;
    }
    
    return 0;
}

void PacketSniffer::run()
{
    const u_char* packet;
    struct pcap_pkthdr packetHeader;
    packetCount = 0;
    
    Time start = Time::getCurrentTime();
    
    while (!threadShouldExit())
    {
        packet = pcap_next (handle, &packetHeader);
        
        if (packet == NULL)
        {
            parser.didntGrab();
        }
        else
        {
            packetCount++;
            parser.parsePacket (packetCount, packetHeader, packet);
        }
    }
    
    Time end = Time::getCurrentTime();
    
    RelativeTime total = end - start;
    parser.sessionStats (packetCount, total);
}

Array<String> PacketSniffer::getAvailableDeviceTypes()
{
    return deviceNameArray;
}